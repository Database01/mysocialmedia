<?php
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);

require "bootstrap.php";

use \Symfony\Component\HttpFoundation\Request;
use \Silex\Application;
use \src\Controller\IndexController;
use \src\Controller\MemberController;
use \src\Controller\MediaController;
use \src\Controller\DocumentController;
use \src\Controller\DocumentTypeController;
use \src\Controller\KindController;
use \src\Controller\LoanController;
use \src\Controller\SearchController;
use \src\Controller\AmazonController;
use \src\Controller\StatisticController;
use \src\Controller\ActivityController;
use \src\Controller\WishController;
use \src\Controller\OAuthController;
use \src\Controller\GroupController;
use \src\Controller\GroupMemberController;

$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array('twig.path' => __DIR__.'/src/Template'));

if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', str_replace("/index.php", "", $_SERVER['SCRIPT_NAME']));
}

/*-----------------------*\
         Index
\*-----------------------*/

$app->get('/', function(Request $request, Application $app) {
    $controller = new IndexController($request, $app);
    return $controller->index();
})->bind('index');


/*-----------------------*\
         API
\*-----------------------*/

$api = $app['controllers_factory'];

/*-----------------------*\
         Middlewares
\*-----------------------*/

$checkId = function (Request $request, Application $app) {
    $data = array();
    if(!isset($_SESSION['id'])) {
        $data['message'] = "Connexion nécessaire";
        return $app->json($data, 500);
    }
};

$checkAdmin = function (Request $request, Application $app) {
    $data = array();
    if(!isset($_SESSION['admin'])) {
        $data['message'] = "Droits administrateurs nécessaires";
        return $app->json($data, 500);
    }
};

/*-----------------------*\
         SearchAmazon
\*-----------------------*/

$search = $app['controllers_factory'];
$search->get('/',function(Request $request, Application $app){
    $controller = new \src\Controller\AmazonController($request,$app);
    return $controller->search();
})->before($checkId);
$api->mount('/search',$search);

/*-----------------------*\
         Utilisateurs
\*-----------------------*/


$api->get('/members', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->get();
})->before($checkAdmin);

$member = $app['controllers_factory'];


$member->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/register', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->register();
});

$member->post('/login', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->login();
});

$member->post('/logout', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->logout();
})->before($checkId);

$member->get('/profile', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->getProfile();
})->before($checkId);

$member->get('/admin', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->isAdmin();
})->before($checkAdmin);

$member->get('/session', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->isLogged();
})->before($checkId);

$member->get('/search', function(Request $request, Application $app) {
    $controller = new SearchController($request, $app);
    return $controller->memberSearch();
})->before($checkId);

$member->get('/contacts', function(Request $request, Application $app) {
    $controller = new MemberController($request, $app);
    return $controller->getContacts();
})->before($checkId);

$member->get('/{id}/contacts', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->getContacts($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/contacts/latest', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->getLatestContacts($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/statistics/dashboard', function (Request $request, Application $app, $id) {
    $controller = new StatisticController($request, $app);
    return $controller->dashboardStatistics($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/statistics/public', function (Request $request, Application $app, $id) {
    $controller = new StatisticController($request, $app);
    return $controller->publicStatistics($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/medias/public', function (Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->getPublic($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{memberId}/contacts/{id}/add', function(Request $request, Application $app, $memberId, $id) {
    $controller = new MemberController($request, $app);
    return $controller->addContact($id, $memberId);
})->assert('id', '\d+')->assert('memberId', '\d+')->before($checkId);

$member->delete('/{memberId}/contacts/{id}', function(Request $request, Application $app, $memberId, $id) {
    $controller = new MemberController($request, $app);
    return $controller->deleteContact($id, $memberId);
})->assert('id', '\d+')->assert('memberId', '\d+')->before($checkId);

$member->post('/{id}/name', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->editName($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{id}/mail', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->editMail($id);
})->assert('id', '\d+')->before($checkAdmin);

$member->get('/loans/{id}/latest', function(Request $request, Application $app, $id) {
    $controller = new LoanController($request, $app);
    return $controller->getLatest($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/documents', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->getDocuments($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{id}/documents/{documentId}/add', function(Request $request, Application $app, $id, $documentId) {
    $controller = new MemberController($request, $app);
    return $controller->addDocument($id, $documentId);
})->assert('id', '\d+')->assert('documentId', '\d+')->before($checkId);

$member->post('/{id}/documents/{documentId}/remove', function(Request $request, Application $app, $id, $documentId) {
    $controller = new MemberController($request, $app);
    return $controller->removeDocument($id, $documentId);
})->assert('id', '\d+')->assert('documentId', '\d+')->before($checkId);

$member->post('/{id}/documents/latest', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->getMines($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{id}/documents/latest/partial', function(Request $request, Application $app, $id) {
    $controller = new MemberController($request, $app);
    return $controller->getLatestMines($id);
})->assert('id', '\d+')->before($checkId);

$member->get('/{id}/wishes', function(Request $request, Application $app, $id) {
    $controller = new WishController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{id}/wishes', function(Request $request, Application $app, $id) {
    $controller = new WishController($request, $app);
    return $controller->add($id);
})->assert('id', '\d+')->before($checkId);

$member->post('/{id}/wishes/collection', function(Request $request, Application $app, $id) {
    $controller = new WishController($request, $app);
    return $controller->addToCollection($id);
})->assert('id', '\d+')->before($checkId);

$member->delete('/{id}/wishes/{documentId}', function(Request $request, Application $app, $id, $documentId) {
    $controller = new WishController($request, $app);
    return $controller->delete($id, $documentId);
})->assert('id', '\d+')->assert('documentId', '\d+')->before($checkId);

$member->get('/ghostmembers/search',function(Request $request, Application $app){
    $controller = new \src\Controller\GhostMemberController($request,$app);
    return $controller->search();
})->before($checkId);

$member->get('/{id}/comments/latest',function(Request $request, Application $app,$id){
    $controller = new MemberController($request,$app);
    return $controller->getLatestComments($id);
})->assert('id','\d+')->before($checkId);

$api->mount('/members', $member);


/*-----------------------*\
         Medias
\*-----------------------*/

$api->get('/medias', function(Request $request, Application $app) {
    $controller = new MediaController($request, $app);
    return $controller->get();
})->before($checkId);

$media = $app['controllers_factory'];

$media->get('/details', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->getAll();
})->before($checkId);

$media->post('/add', function(Request $request, Application $app) {
    $controller = new MediaController($request, $app);
    return $controller->add();
})->before($checkId);

$media->delete('/{id}/delete', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkId);

$media->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$media->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->edit($id);
})->assert('id', '\d+')->before($checkId);

$media->post('/{id}/name', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->setName($id);
})->assert('id', '\d+')->before($checkId);

$media->post('/{id}/public', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->setPublic($id);
})->assert('id', '\d+')->before($checkId);

$media->post('/{id}/private', function(Request $request, Application $app, $id) {
    $controller = new MediaController($request, $app);
    return $controller->setPrivate($id);
})->assert('id', '\d+')->before($checkId);

$media->post('/{id}/document/{documentId}/add', function(Request $request, Application $app, $id, $documentId) {
    $controller = new MediaController($request, $app);
    return $controller->addDocument($id, $documentId);
})->assert('id', '\d+')->assert('documentId', '\d+')->before($checkId);

$media->post('/{id}/document/{documentId}/remove', function(Request $request, Application $app, $id, $documentId) {
    $controller = new MediaController($request, $app);
    return $controller->removeDocument($id, $documentId);
})->assert('id', '\d+')->assert('documentId', '\d+')->before($checkId);

$api->mount('/medias', $media);


/*-----------------------*\
         Documents
\*-----------------------*/

$api->get('/documents', function(Request $request, Application $app) {
    $controller = new DocumentController($request, $app);
    return $controller->get();
})->before($checkId);

$document = $app['controllers_factory'];

$document->post('/add', function(Request $request, Application $app) {
    $controller = new DocumentController($request, $app);
    return $controller->add();
})->before($checkAdmin);

$document->get('/search', function(Request $request, Application $app) {
    $controller = new SearchController($request, $app);
    return $controller->documentSearch();
})->before($checkId);

$document->get('/search/amazon', function(Request $request, Application $app) {
    $controller = new AmazonController($request, $app);
    return $controller->search();
})->before($checkId);

$document->post('/search/asin/{asin}', function(Request $request, Application $app, $asin) {
    $controller = new AmazonController($request, $app);
    return $controller->getProductByASIN($asin);
})->before($checkId);

$document->post('/latest', function(Request $request, Application $app) {
    $controller = new DocumentController($request, $app);
    return $controller->getLatest();
})->before($checkId);

$document->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$document->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->edit($id);
})->assert('id', '\d+')->before($checkAdmin);

$document->delete('/{id}/delete', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkAdmin);


/*-----------------------*\
         Document Types
\*-----------------------*/

$document->get('/types', function(Request $request, Application $app) {
    $controller = new DocumentTypeController($request, $app);
    return $controller->get();
})->before($checkId);

$documentType = $app['controllers_factory'];

$documentType->post('/add', function(Request $request, Application $app) {
    $controller = new DocumentTypeController($request, $app);
    return $controller->add();
})->before($checkAdmin);

$documentType->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new DocumentTypeController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$documentType->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new DocumentTypeController($request, $app);
    return $controller->edit($id);
})->assert('id', '\d+')->before($checkAdmin);

$documentType->delete('/{id}/delete', function(Request $request, Application $app, $id) {
    $controller = new DocumentTypeController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkAdmin);

$document->mount('/types', $documentType);


/*-----------------------*\
         Document Kinds
\*-----------------------*/

$document->get('/kinds', function(Request $request, Application $app) {
    $controller = new KindController($request, $app);
    return $controller->get();
})->before($checkId);

$kind = $app['controllers_factory'];

$kind->post('/add', function(Request $request, Application $app) {
    $controller = new KindController($request, $app);
    return $controller->add();
})->before($checkAdmin);

$kind->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new KindController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$kind->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new KindController($request, $app);
    return $controller->edit($id);
})->assert('id', '\d+')->before($checkAdmin);

$kind->delete('/{id}/delete', function(Request $request, Application $app, $id) {
    $controller = new KindController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkAdmin);

$document->mount('/kinds', $kind);

$api->mount('/documents', $document);


/*-----------------------*\
         Document Comments
\*-----------------------*/

$document->get('/{id}/comments', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->getComment($id);
})->assert('id', '\d+')->before($checkId);

$document->post('/{id}/comments/latest', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->getLatestComments($id);
})->assert('id', '\d+')->before($checkId);

$document->get('/{id}/comments/{comment}', function(Request $request, Application $app, $id, $comment) {
    $controller = new DocumentController($request, $app);
    return $controller->getComment($id, $comment);
})->assert('id', '\d+')->before($checkId);

$document->post('/{id}/comments/{comment}', function(Request $request, Application $app, $id, $comment) {
    $controller = new DocumentController($request, $app);
    return $controller->editComment($id, $comment);
})->assert('id', '\d+')->assert('comment', '\d+')->before($checkId);

$document->post('/{id}/comments/add', function(Request $request, Application $app, $id) {
    $controller = new DocumentController($request, $app);
    return $controller->addComment($id);
})->assert('id', '\d+')->assert('comment', '\d+')->before($checkId);

$document->delete('/{id}/comments/{comment}', function(Request $request, Application $app, $id, $comment) {
    $controller = new DocumentController($request, $app);
    return $controller->deleteComment($id,$comment);
})->assert('id', '\d+')->assert('comment', '\d+')->before($checkId);

/*-----------------------*\
         Loans
\*-----------------------*/

$api->get('/loans', function(Request $request, Application $app) {
    $controller = new LoanController($request, $app);
    return $controller->get();
})->before($checkId);

$loan = $app['controllers_factory'];

$loan->post('/add', function(Request $request, Application $app) {
    $controller = new LoanController($request, $app);
    return $controller->add();
})->before($checkId);

$loan->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new LoanController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$loan->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new LoanController($request, $app);
    return $controller->editOne($id);
})->assert('id', '\d+')->before($checkId);

$loan->post('/{id}/delete', function(Request $request, Application $app, $id) {
    $controller = new LoanController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkId);

$api->mount('/loans', $loan);


/*-----------------------*\
         OAuth
\*-----------------------*/

$oauth = $app['controllers_factory'];

$oauth->get('/signin',function(Request $request, Application $app){
    $controller = new OAuthController($request,$app);
    return $controller->get();
});

$oauth->post('/signin/google',function(Request $request, Application $app){
    $controller = new OAuthController($request,$app);
    return $controller->signinGoogle();
});
$oauth->post('/signin/twitter',function(Request $request, Application $app){
    $controller = new OAuthController($request,$app);
    return $controller->signinTwitter();
});
$oauth->post('/signin/facebook',function(Request $request, Application $app){
    $controller = new OAuthController($request,$app);
    return $controller->signinFacebook();
});

$app->mount('/oauth', $oauth);


/*-----------------------*\
         Activities
\*-----------------------*/

$activity = $app['controllers_factory'];

$activity->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new ActivityController($request, $app);
    return $controller->getLatest($id);
})->assert('id', '\d+')->before($checkId);

$api->mount('/activity', $activity);


/*-----------------------*\
          Groups
\*-----------------------*/

$api->get('/groups', function(Request $request, Application $app) {
   $controller = new GroupController($request, $app);
    return $controller->get();
})->before($checkId);

$groups = $app['controllers_factory'];

$groups->post('/add', function(Request $request, Application $app) {
    $controller = new GroupController($request, $app);
    return $controller->add();
})->before($checkId);

$groups->get('/{id}', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->get($id);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/edit', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->edit($id);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/editType', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->editType($id);
})->assert('id', '\d+')->before($checkId);

$groups->get('/types', function(Request $request, Application $app) {
    $controller = new GroupController($request, $app);
    return $controller->getTypes();
})->before($checkId);

$groups->delete('/{id}', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->delete($id);
})->assert('id', '\d+')->before($checkId);


/*-----------------------*\
         Group Members
\*-----------------------*/

$groups->post('/{id}/members', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->getLatestMembers($id);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/members/add', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->addMember($id);
})->assert('id', '\d+')->before($checkId);

$groups->get('/{id}/members/{member}', function(Request $request, Application $app, $id, $member) {
    $controller = new GroupController($request, $app);
    return $controller->getMembers($id, $member);
})->assert('id', '\d+')->assert('user', '\d+')->before($checkId);

$groups->delete('/{id}/members/{member}', function(Request $request, Application $app, $id, $member) {
    $controller = new GroupController($request, $app);
    return $controller->deleteMember($id, $member);
})->assert('id', '\d+')->assert('member', '\d+')->before($checkId);

$groups->post('/{id}/members/{member}', function(Request $request, Application $app, $id, $member) {
    $controller = new GroupController($request, $app);
    return $controller->editMember($id, $member);
})->assert('id', '\d+')->assert('member', '\d+')->before($checkId);

$groups->post('/{id}/banned', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->getLatestBanned($id);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/banned/add', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->addBanned($id);
})->assert('id', '\d+')->before($checkId);

$groups->delete('/{id}/banned/{ban}', function(Request $request, Application $app, $id, $ban) {
    $controller = new GroupController($request, $app);
    return $controller->deleteBanned($id, $ban);
})->before($checkId);


/*-----------------------*\
         Group Posts
\*-----------------------*/

$groups->get('/{id}/posts', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->getPost($id);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/posts/latest', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->getLatestPosts($id);
})->assert('id', '\d+')->before($checkId);

$groups->get('/{id}/posts/{post}', function(Request $request, Application $app, $id, $post) {
    $controller = new GroupController($request, $app);
    return $controller->getPost($id, $post);
})->assert('id', '\d+')->before($checkId);

$groups->post('/{id}/posts/{post}/edit', function(Request $request, Application $app, $id, $post) {
    $controller = new GroupController($request, $app);
    return $controller->editPost($id, $post);
})->assert('id', '\d+')->assert('post', '\d+')->before($checkId);

$groups->post('/{id}/posts/add', function(Request $request, Application $app, $id) {
    $controller = new GroupController($request, $app);
    return $controller->addPost($id);
})->before($checkId);

$groups->delete('/{id}/posts/{post}', function(Request $request, Application $app, $id, $post) {
    $controller = new GroupController($request, $app);
    return $controller->deletePost($id,$post);
})->before($checkId);


/*-----------------------*\
         Group Search
\*-----------------------*/

$groups->get('/search', function(Request $request, Application $app) {
    $controller = new SearchController($request, $app);
    return $controller->groupSearch();
})->before($checkId);

$groups->post('/{id}/members/search', function(Request $request, Application $app, $id) {
    $controller = new SearchController($request, $app);
    return $controller->groupMemberSearch($id, true);
})->before($checkId);

$groups->post('/{id}/banned/search', function(Request $request, Application $app, $id) {
    $controller = new SearchController($request, $app);
    return $controller->groupBannedSearch($id, true);
})->before($checkId);

$api->mount('/groups', $groups);


$app->mount('/api', $api);


/*-----------------------*\
    Matches all routes
\*-----------------------*/

$app->match('/{url}', function(Request $request, Application $app){

    $controller = new IndexController($request, $app);
    return $controller->app();
})->assert('url', '.+');


$app->run();