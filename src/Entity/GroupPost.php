<?php
namespace src\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\GeneratedValue;


/**
 * @Entity(repositoryClass="src\Repository\GroupPostRepository")
 * @HasLifecycleCallbacks
 * @Table(name="group_post")
 */
class GroupPost
{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var Group
     *
     *
     * @ManyToOne(targetEntity="Group", inversedBy="groupPosts")
     */
    protected $group;


    /**
     * @var \DateTime
     *
     *
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="deleted_at", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="authorPosts")
     */
    protected $author;

    /**
     * @var string
     *
     * @Column(name="message", nullable=true)
     */
    protected $message;


    public function __construct()
    {

    }

    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'group' => $this->getGroup()->toArray(),
            'author' => $this->getAuthor()->toArray(),
            'createdAt' => $this->getCreatedAt()->format('H:i:s d-m-Y'),
            'updatedAt' => $this->getUpdatedAt()->format('H:i:s d-m-Y'),
            'message' => $this->getMessage()
        );
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @PrePersist
     */
    public function onCreate()
    {
        $this->createdAt = new \DateTime("now");
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GroupPost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return GroupPost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return GroupPost
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return GroupPost
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set group
     *
     * @param \src\Entity\Group $group
     * @return GroupPost
     */
    public function setGroup(\src\Entity\Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \src\Entity\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set author
     *
     * @param \src\Entity\Member $author
     * @return GroupPost
     */
    public function setAuthor(\src\Entity\Member $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \src\Entity\Member 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
