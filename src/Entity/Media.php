<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Entity(repositoryClass="src\Repository\MediaRepository")
 * @HasLifecycleCallbacks
 * @Table(name="medias")
 */
class Media
{
    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Column(type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var bool
     *
     * @Column(type="boolean")
     */
    protected $public;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="DocumentMedia", mappedBy="media", cascade={"persist", "remove"})
     */
    protected $documentMedias;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Activity", mappedBy="media", cascade={"persist", "remove"})
     */
    protected $activities;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="medias")
     */
    protected $member;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="deleted_at", nullable=true)
     */
    protected $deletedAt;

    public function __construct() {
        $this->documentMedias = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate() {
        $this->updatedAt = new \DateTime("now");
    }

    public function toArray() {
        $a = array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'public' => $this->getPublic(),
        );

        $b = $this->getDocumentMedias();
        if($b != null) {
            foreach ($this->getDocumentMedias() as $documentMedia) {
                $a['documents'][] = $documentMedia->getDocument()->toArray();
            }
        }

        if(!array_key_exists('documents',$a) || sizeof($a['documents']) == 0) {
            $a['documents'] = [];
        }

        return $a;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return Media
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Media
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Media
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set member
     *
     * @param Member $member
     * @return Media
     */
    public function setMember(Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return Member 
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Media
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add documentMedias
     *
     * @param DocumentMedia $documentMedias
     * @return Media
     */
    public function addDocumentMedia(DocumentMedia $documentMedias)
    {
        $this->documentMedias[] = $documentMedias;

        return $this;
    }

    /**
     * Remove documentMedias
     *
     * @param DocumentMedia $documentMedias
     */
    public function removeDocumentMedia(DocumentMedia $documentMedias)
    {
        $this->documentMedias->removeElement($documentMedias);
    }

    /**
     * Get documentMedias
     *
     * @return Collection
     */
    public function getDocumentMedias()
    {
        return $this->documentMedias;
    }

    /**
     * Add activities
     *
     * @param \src\Entity\Activity $activities
     * @return Media
     */
    public function addActivity(\src\Entity\Activity $activities)
    {
        $this->activities[] = $activities;

        return $this;
    }

    /**
     * Remove activities
     *
     * @param \src\Entity\Activity $activities
     */
    public function removeActivity(\src\Entity\Activity $activities)
    {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getActivities()
    {
        return $this->activities;
    }
}
