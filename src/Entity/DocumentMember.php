<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\DocumentMemberRepository")
 * @HasLifecycleCallbacks
 * @Table(name="document_member")
 */
class DocumentMember
{
    /**
     * @var Document
     *
     * @Id
     * @ManyToOne(targetEntity="Document", inversedBy="documentMembers")
     */
    protected $document;

    /**
     * @var Member
     *
     * @Id
     * @ManyToOne(targetEntity="Member", inversedBy="documentMembers")
     */
    protected $member;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DocumentMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set document
     *
     * @param \src\Entity\Document $document
     * @return DocumentMember
     */
    public function setDocument(\src\Entity\Document $document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return \src\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set member
     *
     * @param \src\Entity\Member $member
     * @return DocumentMember
     */
    public function setMember(\src\Entity\Member $member)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return \src\Entity\Member 
     */
    public function getMember()
    {
        return $this->member;
    }
}
