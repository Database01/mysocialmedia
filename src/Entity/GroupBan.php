<?php
namespace src\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\GeneratedValue;


/**
 * @Entity(repositoryClass="src\Repository\GroupBanRepository")
 * @HasLifecycleCallbacks
 * @Table(name="group_ban")
 */
class GroupBan
{

    /**
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var Group
     *
     *
     * @ManyToOne(targetEntity="Group", inversedBy="groupBans")
     */
    protected $group;

    /**
     * @var Member
     *
     *
     * @ManyToOne(targetEntity="Member", inversedBy="groupBans")
     */
    protected $member;

    /**
     * @var \DateTime
     *
     *
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="ended_at", nullable=true)
     */
    protected $endedAt;

    /**
     * @var \DateTime
     *
     * @Column(type="datetime", name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var Member
     *
     * @ManyToOne(targetEntity="Member", inversedBy="authorBans")
     */
    protected $author;

    /**
     * @var string
     *
     * @Column(name="message", nullable=true)
     */
    protected $message;

    public function __construct()
    {

    }

    public function toArray()
    {
        $res = array();
        $res['id']=$this->getId();
        $res['group']=$this->getGroup()->toArray();
        $res['member']=$this->getMember()->toArray();
        $res['createdAt']=$this->getCreatedAt()->format('H:i:s d-m-Y');
        $res['updatedAt']=$this->getCreatedAt()->format('H:i:s d-m-Y');
        $res['endedAt']=null;
        if ($this->getEndedAt() != null) {
            $res['endedAt']=$this->getCreatedAt()->format('H:i:s d-m-Y');
        }
        $res['author']=$this->getAuthor()->toArray();
        $res['message']=null;
        if ($this->getMessage() != null) {
            $res['message']=$this->getMessage();
        }

        return $res;
    }

    /**
     * @PrePersist @PreUpdate
     */
    public function onUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    /**
     * @PrePersist
     */
    public function onCreate()
    {
        $this->createdAt = new \DateTime("now");
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GroupBan
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set endedAt
     *
     * @param \DateTime $endedAt
     * @return GroupBan
     */
    public function setEndedAt($endedAt)
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    /**
     * Get endedAt
     *
     * @return \DateTime 
     */
    public function getEndedAt()
    {
        return $this->endedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return GroupBan
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return GroupBan
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set group
     *
     * @param \src\Entity\Group $group
     * @return GroupBan
     */
    public function setGroup(\src\Entity\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \src\Entity\Group 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set member
     *
     * @param \src\Entity\Member $member
     * @return GroupBan
     */
    public function setMember(\src\Entity\Member $member = null)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return \src\Entity\Member 
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set author
     *
     * @param \src\Entity\Member $author
     * @return GroupBan
     */
    public function setAuthor(\src\Entity\Member $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \src\Entity\Member 
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
