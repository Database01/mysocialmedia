<?php

namespace src\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping;

/**
 * @Entity(repositoryClass="src\Repository\WishRepository")
 * @HasLifecycleCallbacks
 * @Table(name="wish")
 */
class Wish
{
    /**
     * @var Document
     *
     * @Id
     * @ManyToOne(targetEntity="Document", inversedBy="wishes")
     */
    protected $document;

    /**
     * @var Member
     *
     * @Id
     * @ManyToOne(targetEntity="Member", inversedBy="wishes")
     */
    protected $member;

    /**
     * @var \DateTime
     * @Column(type="datetime", name="created_at")
     */
    protected $createdAt;


    public function __construct() {
        $this->createdAt = new \DateTime("now");
    }

    public function toArray() {

        return $this->getDocument()->toArray();

    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Wish
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set document
     *
     * @param \src\Entity\Document $document
     * @return Wish
     */
    public function setDocument(\src\Entity\Document $document) {
        $this->document = $document;
    }

    /**
     * Get document
     *
     * @return \src\Entity\Document 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set member
     *
     * @param \src\Entity\Member $member
     * @return Wish
     */
    public function setMember(\src\Entity\Member $member)
    {
        $this->member = $member;

        return $this;
    }

    /**
     * Get member
     *
     * @return \src\Entity\Member 
     */
    public function getMember()
    {
        return $this->member;
    }
}
