<?php

namespace src\Controller;

use src\AbstractController;
use src\View\Index;
use src\View\Home;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController {

    public function index() {
        $view = new Index();
        $view->addVar('title', 'Accueil');
        $view->addVar('session', $_SESSION);
        return $view->render();
    }

    public function app() {
        $view = new Home();
        $view->addVar('title', 'Accueil');
        $view->addVar('session', $_SESSION);
        return $view->render();
    }
}