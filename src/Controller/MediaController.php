<?php


namespace src\Controller;

use src\AbstractController;
use src\Entity\DocumentMedia;
use src\Entity\Media;
use Symfony\Component\Validator\Constraints as Assert;

class MediaController extends AbstractController {

    public function get($id = null) {
        $data = array();
        try {
            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);

            $data['medias'] = [];
            if($id == null) {
                $medias = $this->em->getRepository('src\Entity\Media')->findBy(array('member' => $member));
                foreach ($medias as $media) {
                    $data['medias'][] = $media->toArray();
                }
            } else {
                $media = $this->em->getRepository('src\Entity\Media')->findOneBy(array('member' => $member, 'id' => $id));
                if($media == null) {
                    $data['message'] = "Media not found";
                    return $this->app->json($data, 500);
                }
                $data['medias'] = $media->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function getPublic($id) {
        $data = array();
        try {
            $member = $this->em->getRepository('src\Entity\Member')->find($id);

            $data['medias'] = [];

            $medias = $this->em->getRepository('src\Entity\Media')->findBy(array('member' => $member, "public" => 1));
            foreach ($medias as $media) {
                $data['medias'][] = $media->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function add() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $media = new Media();
            $this->sanitize($param, $media, $errors);

            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);

            $media->setMember($member);
            if(property_exists($param, "public") && $param->public == true) {
                $media->setPublic(true);
            } else {
                $media->setPublic(false);
            }

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($media);
            $this->em->flush($media);

            if(property_exists($param, "documents")) {
                foreach ($param->documents as $doc) {
                    $documentMedia = new DocumentMedia();
                    $document = $this->em->getRepository('src\Entity\Document')->find($doc->id);
                    if($document == null) {
                        continue;
                    }
                    $documentMedia->setMedia($media);
                    $documentMedia->setDocument($document);
                    $this->em->persist($documentMedia);
                }
            }

            $this->em->flush();

            if($media->getPublic() == true) {
                $activity = new ActivityController($this->request, $this->app);
                $activity->addActivity($member, $media, 4);
            }

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);

    }

    public function edit($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();

            $media = $this->em->getRepository('src\Entity\Media')->find($id);
            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            if(property_exists($param, "public") && $param->public == true) {
                $media->setPublic(true);
            } else {
                $media->setPublic(false);
            }

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($media);

            if(property_exists($param, "documents")) {
                foreach ($param->documents as $doc) {
                    $documentMedia = new DocumentMedia();
                    $document = $this->em->getRepository('src\Entity\Document')->find($doc->id);
                    if($document == null) {
                        continue;
                    }

                    $dm = $this->em->getRepository('src\Entity\DocumentMedia')->findOneBy(array("document" => $document, "media" => $media));

                    if($dm != null) {
                        continue;
                    }

                    $documentMedia->setMedia($media);
                    $documentMedia->setDocument($document);
                    $this->em->persist($documentMedia);
                }
            }

            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function setName($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $media = $this->em->getRepository('src\Entity\Media')->find($id);

            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            if(!property_exists($param, "name")) {
                throw new \Exception("Name not found");
            }

            $this->sanitize($param, $media, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($media);

            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function setPublic($id) {
        $data = array();

        try {
            $media = $this->em->getRepository('src\Entity\Media')->find($id);

            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }


            $media->setPublic(true);

            $this->em->flush();

            $activity = new ActivityController($this->request, $this->app);
            $activity->addActivity($media->getMember(), $media, 6);

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function setPrivate($id) {
        $data = array();

        try {
            $media = $this->em->getRepository('src\Entity\Media')->find($id);

            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $media->setPublic(false);

            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }


    public function addDocument($id, $documentId) {
        $data = array();

        try {
            $media = $this->em->getRepository('src\Entity\Media')->find($id);

            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $documentMedia = new DocumentMedia();
            $document = $this->em->getRepository('src\Entity\Document')->find($documentId);
            if($document == null) {
                throw new \Exception("Document not found");
            }

            $dm = $this->em->getRepository('src\Entity\DocumentMedia')->findOneBy(array("document" => $document, "media" => $media));
            if($dm != null) {
                throw new \Exception("DocumentMedia already exists");
            }

            $documentMedia->setMedia($media);
            $documentMedia->setDocument($document);
            $this->em->persist($documentMedia);
            $this->em->flush();

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function removeDocument($id, $documentId) {
        $data = array();

        try {
            $media = $this->em->getRepository('src\Entity\Media')->find($id);
            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($documentId);
            if($document == null) {
                throw new \Exception("Document not found");
            }

            $documentMedia = $this->em->getRepository('src\Entity\DocumentMedia')->findOneBy(array("document" => $document, "media" => $media));
            if($documentMedia == null) {
                throw new \Exception("DocumentMedia not found");
            }

            $this->em->remove($documentMedia);
            $this->em->flush();

        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }



    public function delete($id) {
        $data = array();
        try {
            $media = $this->em->getRepository('src\Entity\Media')->find($id);

            if($media == null) {
                throw new \Exception("Media not found");
            }

            if($_SESSION['id'] != $media->getMember()->getId() && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $this->em->remove($media);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function sanitizeName($name, Media &$media) {
        $media->setName(htmlspecialchars(filter_var($name), FILTER_SANITIZE_STRING));
    }

    public function sanitize($data, &$media, &$errors) {
        if (isset($data->name) && strlen($data->name) > 0) {
            $this->sanitizeName($data->name, $media);
        } else {
            $errors['name']="Le champ name est obligatoire";
        }
    }
}