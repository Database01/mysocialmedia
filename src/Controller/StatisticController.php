<?php

namespace src\Controller;

use src\AbstractController;
use Symfony\Component\Validator\Constraints as Assert;

class StatisticController extends AbstractController {

    public function dashboardStatistics($memberId) {
        $data = array();
        try {

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);
            if ($member == null) {
                throw new \Exception("Member not found");
            }

            $documentMembers = $this->em->getRepository('src\Entity\DocumentMember')->findBy(array("member" => $member));
            $data['statistics']['documents'] = count($documentMembers);

            $medias = $member->getMedias();
            $data['statistics']['medias'] = count($medias);

            $contacts = $member->getContacts();
            $data['statistics']['contacts'] = count($contacts);

            $borrows = $this->em->getRepository('src\Entity\Loan')->findBy(array("borrower" => $member));
            $data['statistics']['borrows'] = count($borrows);

            $lends = $this->em->getRepository('src\Entity\Loan')->findBy(array("lender" => $member));
            $data['statistics']['lends'] = count($lends);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function publicStatistics($memberId) {
        $data = array();
        try {

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);
            if ($member == null) {
                throw new \Exception("Member not found");
            }

            $documentMembers = $this->em->getRepository('src\Entity\DocumentMember')->findBy(array("member" => $member));
            $data['statistics']['documents'] = count($documentMembers);

            $medias = $member->getPublicMedias();
            $data['statistics']['medias'] = count($medias);

            $contacts = $member->getContacts();
            $data['statistics']['contacts'] = count($contacts);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

}