<?php


namespace src\Controller;

use src\AbstractController;
use src\Entity\DocumentType;
use src\Entity\Kind;
use Symfony\Component\Validator\Constraints as Assert;

class KindController extends AbstractController {

    public function get($id = null) {
        $data = array();
        try {
            if ($id === null) {
                $kinds = $this->em->getRepository('src\Entity\Kind')->findAll();
                foreach ($kinds as $kind) {
                    $data['kinds'][] = $kind->toArray();
                }
            } else {
                $kind = $this->em->find('src\Entity\Kind', $id);
                if($kind == null) {
                    throw new \Exception('Kind not found');
                }
                $data['kinds'] = $kind->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function add() {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $kind = new Kind();
            $this->sanitize($param, $kind, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($kind);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function edit($id) {
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            $errors = array();
            $kind = $this->em->getRepository('src\Entity\Kind')->find($id);

            if($kind == null) {
                throw new \Exception("Kind not found");
            }

            $this->sanitize($param, $kind, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($kind);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function delete($id) {
        $data = array();
        try {
            $kind = $this->em->getRepository('src\Entity\Kind')->find($id);

            if($kind == null) {
                throw new \Exception("Kind not found");
            }

            $this->em->remove($kind);
            $this->em->flush();
        } catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function sanitizeName($value, Kind &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 3)));
        if(count($errors) == 0) {
            $object->setName(htmlspecialchars(filter_var($value), FILTER_SANITIZE_STRING));
        } else {
            error_log(count($errors));
        }
    }
    public function sanitize($data, &$object, &$errors) {
        $this->sanitizeName($data->name, $object, $errors);
    }
}