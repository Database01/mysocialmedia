<?php
namespace src\Controller;

use src\AbstractController;
use src\Entity\Group;
use src\Entity\GroupBan;
use src\Entity\GroupMember;
use src\Entity\GroupPost;
use src\Entity\Member;
use Symfony\Component\Validator\Constraints as Assert;


class GroupController extends AbstractController {

    private $default_member_level = 3;

    public function get($id = null) {
        $data = array();
        try {
            $bans=null;
            if (array_key_exists('id', $_SESSION)) {
                $bans = $this->em->getRepository('src\Entity\GroupBan')->findBy(array('member' => $_SESSION['id']));
            }
            $data['groups']=array();
            if($id == null) {
                $groups = $this->em->getRepository('src\Entity\Group')->findAll();
                foreach ($groups as $group) {
                    $skip=false;
                    if ($bans != null) {
                        foreach ($bans as $ban) {
                            if ($ban->getGroup()->getId() == $group->getId()) {
                                $skip=true;
                                break;
                            }
                        }
                    }
                    if (!$skip) {
                        $data['groups'][] = $group->toArray();
                    }
                }
            }
            else {
                $group=$this->checkGroup($id);
                if (array_key_exists('id', $_SESSION)) {
                    $bans = $this->em->getRepository('src\Entity\GroupBan')->findBy(array('member' => $_SESSION['id'], 'group' => $id));
                    if (count($bans) > 0) {
                        $data['groups']=array();
                    }
                    else {
                        $data['groups'] = $group->toArray();
                    }
                }
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function add() {

        try {
            $param = $this->checkRequest(array('name', 'type'));

            $errors = array();
            $data = array();
            $group = new Group();
            $this->sanitize($param, $group, $errors);

            if(sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($group);
            $this->em->flush();

            //Adding user to group

            $member = $this->em->getRepository('src\Entity\Member')->find($_SESSION['id']);
            if ($member == null) {
                throw new \Exception("Member not found");
            }

            $right = $this->em->getRepository('src\Entity\GroupRight')->find(1);
            if ($right == null) {
                throw new \Exception("Level not found");
            }

            $groupMember = new GroupMember();
            $groupMember->setGroup($group);
            $groupMember->setMember($member);
            $groupMember->setRight($right);

            $this->em->persist($groupMember);
            $this->em->flush();

            $data['message'] = "Group added";
            $data['group'] = $group->toArray();
            return $this->app->json($data, 201);
        }
        catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function edit($id) {
        try {
            $param = $this->checkRequest(array('name', 'type'));
            $errors = array();
            $group = $this->checkGroup($id);
            $this->checkGroupAuthorizations($id, 2, false);

            $data = array();
            $this->sanitize($param, $group, $errors);

            if (sizeof($errors) > 0) {
                throw new \Exception();
            }

            $this->em->persist($group);
            $this->em->flush();
            $data['message'] = "Group edited";
            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function getTypes() {
        $data=array();
        try {
            $types = $this->em->getRepository('src\Entity\GroupType')->findAll();
            foreach ($types as $type) {
                $data['types'][] = $type->toArray();
            }

            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function editType($id) {
        try {
            $param = $this->checkRequest(array('type'));
            $errors = array();
            $group = $this->checkGroup($id);
            $data = array();
            $this->sanitizeType($param->type, $group, $errors);

            if (sizeof($errors) > 0) {
                throw new \Exception('Error editing group');
            }

            $this->em->persist($group);
            $this->em->flush();
            $data['message'] = "Group edited";
            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function delete($id) {
        try {
            $group = $this->checkGroup($id);
            $this->checkGroupAuthorizations($id, 1, false);
            $data = array();
            $this->em->remove($group);
            $this->em->flush();
            $data['message'] = "Group $id deleted";
            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    // Group Members //

    public function getMembers($id_group, $id_member=null) {
        $data=array();
        try {
            $this->checkGroup($id_group);

            $data['members']=array();

            if ($id_member == null) {
                $members = $this->em->getRepository('src\Entity\GroupMember')->findBy(array('group' => $id_group));
                if ($members == null) {
                    $data['message']="No members in this group";
                    return $this->app->json($data, 200);
                }

                foreach ($members as $member) {
                    //TODO Get ban infos with getBanned(), but must filter to only get ban infos for this group!
                    $data['members'][]=$member->toArray();
                }
            }
            else {
                $member = $this->em->getRepository('src\Entity\GroupMember')->findOneBy(array('group' => $id_group, 'member' => $id_member));
                if ($member == null) {
                    $data['message'] = "Member not in group";
                    return $this->app->json($data, 404);
                }
                //TODO Get ban infos with getBanned(), but must filter to only get ban infos for this group!
                $data['members']=$member->toArray();
            }


            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function getLatestMembers($id_group) {
        $data = array();

        try {
            $param = $this->checkJson($this->request->getContent());
            $data['members'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('m')
                ->from('src\Entity\GroupMember','m')
                ->where('m.group = :idgroup')
                ->orderBy('m.createdAt', 'DESC')
                ->setParameter(':idgroup', $id_group);


            if(array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if(array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();
            foreach ($results as $member) {
                $data['members'][] = $member->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addMember($id_group) {
        $data=array();


        try {
            $body = $this->request->getContent();
            $param = $this->checkJson($body);


            $group=$this->checkGroup($id_group);

            if (array_key_exists('member', $param)) {
                $id_member = $param->member;
                $this->checkGroupAuthorizations($id_group, 2, false);

                $member = $this->em->getRepository('src\Entity\Member')->find($id_member);
                if ($member == null) {
                    $data['message']="Member not found";
                    return $this->app->json($data, 404);
                }
            }
            else {
                $member=$this->checkSelfjoinable($id_group);

                $id_member = $member->getId();
            }

            $level=$this->default_member_level;

            $members = $this->em->getRepository('src\Entity\GroupMember')->findBy(array('group' => $id_group, 'member' => $id_member));
            if (sizeof($members) > 0) {
                $data['message']="Member already in group";
                return $this->app->json($data, 409);
            }


            $right = $this->em->getRepository('src\Entity\GroupRight')->find($level);
            if ($right == null) {
                $data['message']="Level not found";
                return $this->app->json($data, 404);
            }

            $groupMember = new GroupMember();
            $groupMember->setGroup($group);
            $groupMember->setMember($member);
            $groupMember->setRight($right);

            $this->em->persist($groupMember);
            $this->em->flush();

            $data['message']="Member successfully added to group";
            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }


    public function deleteMember($id_group, $id_member) {
        $data = array();
        try {
            $this->checkGroup($id_group);

            $member = $this->checkGroupAuthorizations($id_group, 2, true, $id_member);

            $this->em->remove($member);
            $this->em->flush();

            $data['message'] = "Member removed from group";

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function editMember($id_group, $id_member) {
        $data = array();

        try {
            $param = $this->checkRequest(array('right_id'));
            $right_id=$param->right_id;

            $this->checkGroup($id_group);

            $this->checkGroupAuthorizations($id_group, 2, false);


            $member = $this->em->getRepository('src\Entity\GroupMember')->findOneBy(array('group' => $id_group, 'member' => $id_member));
            if ($member == null) {
                $data['message'] = "Member not in group";
                return $this->app->json($data, 404);
            }

            $right = $this->em->getRepository('src\Entity\GroupRight')->find($right_id);
            if ($right == null) {
                $data['message'] = "Right not found";
                return $this->app->json($data, 404);
            }

            $member->setRight($right);
            

            $this->em->persist($member);
            $this->em->flush();

            $data['message'] = "Member rights updated";

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    // Group Banned members //

    public function getBanned($id_group, $id_member=null) {
        $data=array();
        try {
            $this->checkGroup($id_group);

            $this->checkGroupAuthorizations($id_group, 2, false);

            $data['members']=array();

            if ($id_member == null) {
                $banned = $this->em->getRepository('src\Entity\GroupBan')->findBy(array('group' => $group));

                if ($banned == null) {
                    $data['message']="No banned members in this group";
                    return $this->app->json($data, 200);
                }

                foreach ($banned as $member) {
                    $data['members'][]=$member->toArray();
                }
            }
            else {
                $banned = $this->em->getRepository('src\Entity\GroupBan')->findOneBy(array('group' => $id_group, 'member' => $id_member));
                if ($banned == null) {
                    $data['message'] = "Member not banned in this group";
                    return $this->app->json($data, 200);
                }

                $data['members']=$banned->toArray();
            }


            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function getLatestBanned($id_group) {
        $data = array();

        try {
            $param = $this->checkJson($this->request->getContent());
            $data['members'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('m')
                ->from('src\Entity\GroupBan','m')
                ->where('m.group = :idgroup')
                ->orderBy('m.createdAt', 'DESC')
                ->setParameter(':idgroup', $id_group);


            if(array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if(array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();
            foreach ($results as $key=>$member) {
                $groupmember=$this->em->getRepository('src\Entity\GroupMember')->findOneBy(array('member' => $member->getId(), 'group' => $member->getGroup()->getId()));
                $data['members'][$key]['groupmember'] = null;
                if (!$groupmember == null) {
                    $data['members'][$key]['groupmember'] = $groupmember->toArray();
                }
                $data['members'][$key] = $member->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addBanned($id_group) {
        $data=array();

        try {
            $params = $this->checkRequest(array('member'));
            $id_member = $params->member;
            $group=$this->checkGroup($id_group);

            $author=$this->checkGroupAuthorizations($id_group, 2, false);

            //Member must not necessarily be in the group.
            $member = $this->em->getRepository('src\Entity\Member')->find($id_member);
            if ($member == null) {
                $data['message'] = "Member not found";
                return $this->app->json($data, 200);
            }

            $groupban = new GroupBan();
            $groupban->setMember($member);
            $groupban->setAuthor($author->getMember());
            $groupban->setGroup($group);
            if (array_key_exists('ended_at', $params)) {
                $ended_at=\DateTime::createFromFormat('H:i:s d-m-Y', $params->ended_at);
                $groupban->setEndedAt($ended_at);
            }
            if (array_key_exists('message', $params)) {
                $groupban->setMessage($params->message);
            }

            $this->em->persist($groupban);
            $this->em->flush();

            $data['message']="Member successfully banned";
            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function deleteBanned($id_group, $id_ban) {
        $data = array();

        try {
            $this->checkGroupAuthorizations($id_group, 2, false);

            $this->checkGroup($id_group);

            $ban = $this->em->getRepository('src\Entity\GroupBan')->find($id_ban);
            if($ban == null) {
                $data['message'] = "Ban entry not found";
                return $this->app->json($data, 404);
            }



            $this->em->remove($ban);
            $this->em->flush();

            $data['message'] = "Ban removed";

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    // Group posts //
    public function getPost($id_group, $id_post=null) {
        $data=array();
        try {
            $this->checkGroup($id_group);

            $data['posts']=array();

            if ($id_post == null) {
                $posts = $this->em->getRepository('src\Entity\GroupPost')->findBy(array('group' => $id_group));
                if ($posts == null) {
                    $data['message']="No posts in this group";
                    return $this->app->json($data, 200);
                }

                foreach ($posts as $post) {
                    $data['posts'][]=$post->toArray();
                }
            }
            else {
                $post = $this->em->getRepository('src\Entity\GroupPost')->findOneBy(array('group' => $id_group, 'id' => $id_post));
                if ($post == null) {
                    $data['message'] = "Post not found";
                    return $this->app->json($data, 404);
                }
                $data['posts']=$post->toArray();
            }


            return $this->app->json($data, 200);
        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function getLatestPosts($id_group) {
        $data = array();

        try {
            $param = $this->checkJson($this->request->getContent());
            $data['posts'] = [];

            $qb = $this->em->createQueryBuilder();

            $qb->select('p')
                ->from('src\Entity\GroupPost','p')
                ->where('p.group = :idgroup')
                ->orderBy('p.createdAt', 'DESC')
                ->setParameter(':idgroup', $id_group);


            if(array_key_exists('count', $param) && $param->count != null) {
                $qb->setMaxResults($param->count);
            } else {
                $qb->setMaxResults(10);
            }

            if(array_key_exists('from', $param) && $param->from != null) {
                $qb->setFirstResult($param->from);
            }

            $results = $qb->getQuery()->execute();
            foreach ($results as $post) {
                $data['posts'][] = $post->toArray();
            }
        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addPost($id_group) {
        $data=array();
        try {
            $param = $this->checkRequest(array('message'));
            $message = $param->message;

            $group=$this->checkGroup($id_group);

            $id_author = $_SESSION['id'];
            $author = $this->em->getRepository('src\Entity\Member')->find($id_author);
            if ($author == null) {
                $data['message'] = "Member not found";
                return $this->app->json($data, 400);
            }

            $post = new GroupPost();
            $post->setGroup($group);
            $post->setAuthor($author);
            $post->setMessage($message);

            $this->em->persist($post);
            $this->em->flush();

            $data['message'] = "Post created";
            return $this->app->json($data, 201);

        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function deletePost($id_group, $id_post) {
        $data=array();
        try {

            $this->checkGroup($id_group);

            $post = $this->em->getRepository('src\Entity\GroupPost')->find($id_post);
            if($post == null) {
                $data['message'] = "Post not found";
                return $this->app->json($data, 404);
            }

            $this->checkGroupAuthorizations($id_group, 2, true, $post->getAuthor()->getId());

            $this->em->remove($post);
            $this->em->flush();

            $data['message'] = "Post successfully deleted";
            return $this->app->json($data, 200);

        }
        catch (\Exception $e) {
            $data['message']=$e->getMessage();
            return $this->app->json($data, 500);
        }
    }

    public function editPost($id_group, $id_post) {
        $data = array();
        try {
            $param = $this->checkRequest(array('message'));
            $message = $param->message;

            $this->checkGroup($id_group);

            $post = $this->em->getRepository('src\Entity\GroupPost')->find($id_post);
            if($post == null) {
                $data['message'] = "Post not found";
                return $this->app->json($data, 404);
            }

            $this->checkGroupAuthorizations($id_group, 1, true, $post->getAuthor()->getId());

            $post->setMessage($message);

            $this->em->persist($post);
            $this->em->flush();

            $data['message'] = "Post edited";

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }


    // Authorizations check //


    /**
     * This function checks user authorization with access level, and allows self-doable actions (like editing our own post).
     * This is really badass.
     *
     * @param $id_group int, Id of the group from which to check access rights
     * @param $required_level int, Id of the required level to execute the action
     * @param $self_doable boolean, (true/false) Is the member allowed to self execute the action
     * @param null $id_member int, required if $self_doable is set to true, represents the id of the user to check with current logged user to allow self actions
     * @return Member if $self_doable is set to true
     * @throws \Exception
     */
    private function checkGroupAuthorizations($id_group, $required_level, $self_doable, $id_member=null) {
        if (!array_key_exists('id', $_SESSION)) {
            throw new \Exception("You must be logged in to perform that action");
        }
        $id_author = $_SESSION['id'];

        $user = $this->em->getRepository('src\Entity\Member')->find($id_author);

            $author = $this->em->getRepository('src\Entity\GroupMember')->findOneBy(array('group' => $id_group, 'member' => $id_author));

            if ($author == null && !$user->getAdmin()) {
                throw new \Exception("Author not found in the group");
            }

            if (!$self_doable) {
                if (!$user->getAdmin() && $author->getRight()->getLevel() > $required_level) {
                    throw new \Exception("Member does not have sufficient rights to do that");
                }
                return $author;
            }
            else {
                $member = $this->em->getRepository('src\Entity\GroupMember')->findOneBy(array('group' => $id_group, 'member' => $id_member));
                if ($member == null) {
                    $data['message'] = "Member not in group";
                    return $this->app->json($data, 404);
                }

                if ($author->getRight()->getId() > $required_level && $author->getMember()->getId() != $member->getMember()->getId() && !$user->getAdmin()) {
                    throw new \Exception("Member does not have sufficient rights to do that");
                }

                return $member;
            }
    }

    private function checkSelfjoinable($id_group) {
        if (!array_key_exists('id', $_SESSION)) {
            throw new \Exception("You must be logged in to perform that action");
        }
        $id_user = $_SESSION['id'];

        $user = $this->em->getRepository('src\Entity\Member')->find($id_user);
        if (!$user->getAdmin()) {
            //Check if $user can join group
            $group = $this->checkGroup($id_group);
            if (!$group->getType()->getSelfjoinable()) {
                throw new \Exception("You cannot join this group by yourself.");
            }

            return $user;
        }
        else {
            return $user;
        }
    }

    private function checkGroup($id_group) {
        $group=$this->em->getRepository('src\Entity\Group')->find($id_group);
        if ($group == null) {
            throw new \Exception("Group not found");
        }
        return $group;
    }

    /**
     * Checks if the JSON from the request is well formed, and parses it into an object
     *
     * @param $data Content of the request
     * @return mixed
     * @throws \Exception
     */
    private function checkJson($data) {
        $json = json_decode($data);
        if ($json == null) {
            throw new \Exception("Malformed JSON");
        }
        return $json;
    }

    /**
     * Checks if required parameters are effectively present in a request.
     *
     * @param $data Parsed JSON object
     * @param $params Array, params to check if they are present in the $data
     * @throws \Exception
     */
    private function checkParams($data, $params) {
        foreach ($params as $param) {
            if (!array_key_exists($param, $data)) {
                throw new \Exception("Missing required parameter(s)");
            }
        }
    }

    /**
     * Both checks & parses JSON from the current request, as well as checking the parameters.
     *
     * @param $data Content of the request
     * @param $params Array, params to check if they are present in the $data
     * @return Object
     * @throws \Exception
     */
    private function checkRequest($params) {
        $body = $this->request->getContent();
        $json = $this->checkJson($body);
        $this->checkParams($json, $params);
        return $json;
    }


    /*-----------------------*\
             Sanitizers
    \*-----------------------*/

    public function sanitizeName($value, Group &$object, &$errors) {
        $errors = $this->app['validator']->validateValue($value, new Assert\Length(array('min' => 3)));
        if(count($errors) == 0) {
            $object->setName($value);
        } else {
            $errors[]='Name invalid';
        }
    }

    public function sanitizeType($value, Group &$object, &$errors) {
        try {
            $type=$this->em->getRepository('src\Entity\GroupType')->find($value);

            if ($type == null) {
                throw new \Exception('Type not found');
            }
            $object->setType($type);
        }
        catch (\Exception $e){
            $errors[]=$e->getMessage();
        }
    }

    public function sanitize($data, &$object, &$errors) {
        $this->sanitizeName($data->name, $object, $errors);
        $this->sanitizeType($data->type, $object, $errors);
    }
}