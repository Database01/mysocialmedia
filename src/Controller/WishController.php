<?php

namespace src\Controller;

use src\AbstractController;
use src\Entity\DocumentMember;
use src\Entity\Wish;
use src\Entity\Document;
use src\Entity\Member;

class WishController extends  AbstractController {

    public function get($memberId){
        $data = array();
        try {
            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $data['documents'] = [];

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);

            $wishes = $member->getWishes();
            foreach ($wishes as $wish) {
                $data['documents'][] = $wish->toArray();
            }

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
        return $this->app->json($data, 200);
    }

    public function add($memberId){
        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($param->document);
            if($document == null) {
                throw new \Exception('Document not found');
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);
            if($member == null) {
                throw new \Exception('Member not found');
            }

            $dm = $this->em->getRepository('src\Entity\DocumentMember')->findOneBy(array("document" => $document, "member" => $member));
            if($dm != null) {
                throw new \Exception("DocumentMember already exists");
            }
            if(!array_key_exists('id', $_SESSION)) {
                throw new \Exception('Connexion nécessaire');
            }

            if($memberId != $_SESSION['id']){
                throw new \Exception('You can\'t add wishes for other');
            }

            $wish = new Wish();

            $wish->setDocument($document);
            $wish->setMember($member);
            $this->em->persist($wish);
            $this->em->flush();

            $activity = new ActivityController($this->request, $this->app);
            $activity->addActivity($member, $document, 5);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function addToCollection($memberId) {

        $data = array();
        $body = $this->request->getContent();
        $param = json_decode($body);

        try {
            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($param->document);
            if($document == null) {
                throw new \Exception('Document not found');
            }

            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);
            if($member == null) {
                throw new \Exception('Member not found');
            }

            $wish = $this->em->getRepository('src\Entity\Wish')->findOneBy(array('member' => $member, 'document' => $document));
            if($wish == null){
                throw new \Exception('Wish not found');
            }

            $this->em->remove($wish);
            $this->em->flush();

            $dm = $this->em->getRepository('src\Entity\DocumentMember')->findOneBy(array("document" => $document, "member" => $member));
            if($dm != null) {
                throw new \Exception("DocumentMember already exists");
            }

            $documentMember = new DocumentMember();
            $documentMember->setMember($member);
            $documentMember->setDocument($document);
            $this->em->persist($documentMember);
            $this->em->flush();

            $activity = new ActivityController($this->request, $this->app);
            $activity->addActivity($member, $document, 1);

        } catch (\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }

        return $this->app->json($data, 200);
    }

    public function delete($memberId, $documentId){
        $data = array();

        try {
            $member = $this->em->getRepository('src\Entity\Member')->find($memberId);
            if($member == null) {
                throw new \Exception('Member not found');
            }

            if($_SESSION['id'] != $memberId && !isset($_SESSION['admin'])) {
                throw new \Exception("Forbidden");
            }

            $document = $this->em->getRepository('src\Entity\Document')->find($documentId);
            if($document == null) {
                throw new \Exception('Document not found');
            }

            $wish = $this->em->getRepository('src\Entity\Wish')->findOneBy(array('member' => $member, 'document' => $document));
            if($wish == null){
                throw new \Exception('Wish not found');
            }

            $this->em->remove($wish);
            $this->em->flush();

        }catch(\Exception $e) {
            $data['message'] = $e->getMessage();
            return $this->app->json($data, 500);
        }
        return $this->app->json($data, 200);
    }



}