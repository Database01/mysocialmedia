app.controller('DocumentTypeEditCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'documentTypeService', function ($scope, $location, loginService, $http, $routeParams, documentTypeService) {

    $scope.title = "Modifier un type de document";

    documentTypeService.getOne($routeParams.id)
        .success(function (result) {
            $scope.type = result.types;
        });

    $scope.submit = function () {
        documentTypeService.editOne($routeParams.id, $scope.type)
            .success(function () {
                $location.path('/document/type');
            });
    };

}]);