app.controller('DocumentEditCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'documentService', 'documentTypeService', 'documentKindService', function ($scope, $location, loginService, $http, $routeParams, documentService, documentTypeService, documentKindService) {

    $scope.title = "Modifier un document";

    loadTypes = function () {
        documentTypeService.getAll()
            .success(function (result) {
                $scope.types = result.types;
                for (var i = 0; i < result.types.length; i++) {
                    if(result.types[i].name == $scope.document.type) {
                        result.types[i].selected = true;
                    }
                }
            });

        documentKindService.getAll()
            .success(function (result) {
                $scope.kinds = result.kinds;
            });
    };

    documentService.getOne($routeParams.id)
        .success(function (result) {
            $scope.document = result.documents;
            loadTypes();
        });

    $scope.submit = function () {
        documentService.editOne($routeParams.id, $scope.document)
            .success(function () {
                $location.path('/document');
            });
    };

}]);