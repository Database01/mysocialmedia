app.controller('DocumentKindDeleteCtrl', ['$scope', '$location', 'loginService', '$http', '$routeParams', 'documentKindService', function ($scope, $location, loginService, $http, $routeParams, documentKindService) {

    $scope.title = "Supprimer un genre de document ?";

    documentKindService.getOne($routeParams.id)
        .success(function (result) {
            $scope.kind = result.kinds;
            $scope.title = "Supprimer le genre de document \""+$scope.kind.name+"\" ?";
        });

    $scope.deleteType = function () {
        documentKindService.deleteOne($routeParams.id)
            .success(function () {
                $location.path('/document/kind');
            });
    };

    $scope.cancel = function () {
        $location.path('/document/kind');
    }

}]);