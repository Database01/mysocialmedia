app.controller('DocumentAddCtrl', ['$scope', '$location', 'loginService', '$http', 'documentTypeService', function ($scope, $location, loginService, $http, documentTypeService) {

    $scope.document = null;

    $scope.title = "Créer un document";

    documentTypeService.getAll()
        .success(function (result) {
            $scope.types = result.types;
        });

    $scope.submit = function () {
        console.log($scope.document);
    };

}]);