app.controller('ActivityCtrl', ['$scope', '$location', 'loginService', '$http', '$rootScope', 'memberService', function ($scope, $location, loginService, $http, $rootScope, memberService) {

    var getActivities = function () {
        memberService.getLatestActivities(memberService.getId())
            .success(function (result) {
                $scope.activitiesLoaded = true;
                for (var i = 0; i < result.activities.length; i++) {
                    var activity = result.activities[i];
                    activity.target = {};
                    activity.member.detail = rootpath + "/member/" + activity.member.id;

                    if(activity.document !== undefined) {
                        activity.target.name = activity.document.name;
                        activity.target.detail = rootpath + "/document/" + activity.document.id;
                    }

                    if(activity.media !== undefined) {
                        activity.target.name = activity.media.name;
                        activity.target.detail = rootpath + "/media/" + activity.media.id;
                    }

                    if(activity.contact !== undefined) {
                        activity.target.name = activity.contact.name;
                        activity.target.detail = rootpath + "/member/" + activity.contact.id;
                    }
                }

                $scope.activities = result.activities;
            });
    };


    $scope.$watch(function () {
        return $rootScope.connected;
    }, function (newVal) {
        if(!newVal) {
            return;
        }
        $scope.activities = [];
        getActivities();

    });

    $scope.updateActivities = function () {
        $scope.activitiesLoaded = false;
        $scope.activities = [];
        getActivities();
    };

}]);