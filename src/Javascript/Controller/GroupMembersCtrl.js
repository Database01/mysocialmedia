app.controller('GroupMembersCtrl', ['$scope', '$location', '$http', '$routeParams', 'loginService', 'groupService', 'searchService', 'memberService', '$timeout', '$rootScope', function ($scope, $location, $http, $routeParams, loginService, groupService, searchService, memberService, $timeout, $rootScope) {

    $scope.checkingRights = true;

    refreshGroup = function() {
        groupService.getOne($routeParams.id)
            .success(function (result) {
                result.groups.url = rootpath + "/groups/" + result.groups.id;
                $scope.group = result.groups;

                if (!$scope.group.type.selfjoinable && $scope.loggedMember === undefined) {
                    $location.path(rootpath+"/social");
                    $rootScope.$broadcast('alertEvent', {message:"Vous ne pouvez pas accéder à ce contenu", type:"danger"});
                }
                else {
                    $scope.checkingRights = false;
                    getLatestMembers($routeParams.id);
                }
            });
    };


    getLatestMembers = function(id, from) {
        var query = {};

        if (from === undefined) {
            query=groupService.getLatestMembers(id);
        }
        else {
            query=groupService.getLatestMembers(id, from);
        }
        query.success(function (result) {
            if (from === undefined) {
                $scope.members = result.members;
            }
            else {
                for (var i=0; i<result.members.length; i++) {
                    $scope.members.push(result.members[i]);
                }
            }
            if (result.members.length === 0) {
                $scope.members.canLoad = false;
            } else {
                $scope.members.canLoad = true;
            }
        });
    };

    refreshMember = function() {
        groupService.getOneMember($routeParams.id, memberService.getId())
            .success(function(result) {
                $scope.loggedMember = result.members;
                refreshGroup();
            })
            .error(function(result){
                refreshGroup();
            });
    };

    refreshMember();

    $scope.moreLatestMembers = function () {
        getLatestMembers($routeParams.id, $scope.members.length);
    };

    $scope.join = function() {
        groupService.addMember($routeParams.id)
            .success(function(result) {
                getLatestMembers($routeParams.id);
            });
    };

    $scope.search = "";

    launchSearch = function () {
        var params = {};
        if ($scope.search.length > 3) {
            $scope.searchWorking = true;
            params.name=$scope.search;
            searchService.groupMemberSearch($routeParams.id, params)
                .success(function (result) {
                    $scope.searchWorking = false;
                    $scope.searchResults = {};
                    for (var i = 0; i < result.members.length; i++) {
                        result.members[i].detail = rootpath + "/member/" + result.members[i].id;
                        for (var j = 0; j < result.members[i].groups.length; j++) {
                            if (result.members[i].groups[j].id == $routeParams.id) {
                                result.members[i].inGroup = true;
                                result.members[i].role = result.members[i].groups[j].groupmember.right.name;
                                break;
                            }
                        }
                        //result.searchResults[i].detail = rootpath + "/document/" + result.documents[i].id;
                    }
                    $scope.searchResults = result.members;
                    $scope.searchResults.canLoad = (result.members.length !== 0);
                });
        }
    };

    $scope.moreSearchMembers = function() {
        var params={};
        params.name = $scope.search;
        params.from = $scope.searchResults.length;
        params.count = 10;
        searchService.groupMemberSearch($routeParams.id, params)
            .success(function(result) {
                if (result.members.length > 0) {
                    for(var i = 0; i<result.members.length; i++) {
                        $scope.searchResults.push(result.members[i]);
                    }
                }
                $scope.searchResults.canLoad = (result.members.length !== 0);
            });
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.addMember = function (id) {
        groupService.addMember($routeParams.id, id)
            .success(function (result) {
                for (var i = 0; i < $scope.searchResults.length; i++) {
                    if($scope.searchResults[i].member.id === id) {
                        $scope.searchResults[i].inGroup = true;
                        getLatestMembers($routeParams.id);
                        launchSearch();
                        break;
                    }
                }
            });
    };

    $scope.deleteMember = function (id) {
        groupService.deleteMember($routeParams.id, id)
            .success(function (result) {
                for (var i = 0; i < $scope.members.length; i++) {
                    if($scope.members[i].member.id === id) {
                        $scope.members.splice(i, 1);
                        break;
                    }
                }

                if ($scope.searchResults !== undefined) {
                    for (i = 0; i < $scope.searchResults.length; i++) {
                        if($scope.searchResults[i].member.id === id) {
                            $scope.searchResults[i].inGroup = false;
                            break;
                        }
                    }
                }
            });
    };


}]);
