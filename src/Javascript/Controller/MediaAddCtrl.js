app.controller('MediaAddCtrl', ['$scope', '$location', 'loginService', '$http', 'mediaService', 'searchService', '$timeout', function ($scope, $location, loginService, $http, mediaService, searchService, $timeout) {

    $scope.media = {
        documents : [],
        public: true
    };

    $scope.search = "";

    $scope.filter = 0;

    launchSearch = function () {
        if ($scope.search.length > 3) {
            $scope.searchStarted = true;
            $scope.searchLaunched = true;
            searchService.documentSearch({search: $scope.search})
                .success(function (result) {
                    $scope.searchStarted = false;
                    $scope.documents = {};
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
                    }
                    $scope.documents = result.documents;
                });
        }
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.addDocument = function (id) {
        for (var i = 0; i < $scope.media.documents.length; i++) {
            if($scope.media.documents[i].id === id) {
                return;
            }
        }
        for (i = 0; i < $scope.documents.length; i++) {
            if($scope.documents[i].id === id) {
                $scope.media.documents.push($scope.documents[i]);
                break;
            }
        }
    };

    $scope.removeDocument = function (id) {
        for (var i = 0; i < $scope.media.documents.length; i++) {
            if($scope.media.documents[i].id === id) {
                $scope.media.documents.splice(i, 1);
                break;
            }
        }
    };

    $scope.submit = function () {
        mediaService.addOne($scope.media)
            .success(function () {
                $location.path('/media');
            });

    };

    $scope.showSearch = function () {
        $scope.displaySearch = true;
    };

}]);