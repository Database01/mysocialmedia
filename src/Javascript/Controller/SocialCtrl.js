app.controller('SocialCtrl', ['$scope', '$location', 'loginService', 'groupService', '$http', '$routeParams', 'memberService', 'searchService', '$timeout', function ($scope, $location, loginService, groupService, $http, $routeParams, memberService, searchService, $timeout) {

    $scope.search = "";

    $scope.addGroup = rootpath + "/groups/add";

    $scope.searchMember = "";

    $scope.showSearchM = false;

    $scope.showAddGroup = function () {
        $location.path("/groups/add");
    };

    $scope.showSearchMember = function () {
        $scope.showSearchM = true;
    };

    memberService.getAllContacts(memberService.getId())
        .success(function (result) {
            if(result.contacts != undefined) {
                for (var i = 0; i < result.contacts.length; i++) {
                    result.contacts[i].detail = rootpath + "/member/" + result.contacts[i].id;
                    result.contacts[i].inContacts = true;
                }
            }
            $scope.contacts = result.contacts;
            $scope.contacts.canLoad = ($scope.contacts.length !== 0);
        });


    launchSearch = function () {
        if ($scope.searchMember && $scope.searchMember.length > 3) {
            $scope.contactSearchStarted = true;
            $scope.contactSearchLaunched = true;
            searchService.userSearch({search: $scope.searchMember})
                .success(function (result) {
                    $scope.contactSearchStarted = false;
                    if(result.members && result.members.length > 0) {
                        var memberId = memberService.getId();
                        for (var i = 0; i < result.members.length; i++) {
                            if(result.members[i].id === memberId) {
                                result.members.splice(i, 1);
                            } else {
                                for (var j = 0; j < $scope.contacts.length; j++) {
                                    if($scope.contacts[j].id === result.members[i].id) {
                                        result.members[i].inContacts = true;
                                    }
                                }
                                result.members[i].detail = rootpath+"/member/"+result.members[i].id;
                            }
                        }
                    }
                    $scope.members = result.members;
                    $scope.members.canLoad = ($scope.members.length !== 0);
                });
        }
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('searchMember', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);

    $scope.addContact = function (id) {
        memberService.addContact(id, memberService.getId())
            .success(function (result) {
                for (var i = 0; i < $scope.members.length; i++) {
                    if($scope.members[i].id === id) {
                        $scope.members[i].inContacts = true;
                        $scope.contacts.push($scope.members[i]);
                        break;
                    }
                }
            });
    };

    $scope.removeContact = function (id) {
        memberService.removeContact(id, memberService.getId())
            .success(function (result) {
                for (var i = 0; i < $scope.contacts.length; i++) {
                    if($scope.contacts[i].id === id) {
                        $scope.contacts.splice(i, 1);
                        break;
                    }
                }

                for (i = 0; i < $scope.members.length; i++) {
                    if($scope.members[i].id === id) {
                        $scope.members[i].inContacts = false;
                        break;
                    }
                }
            });
    };

    groupService.getAll()
        .success(function (result) {
            for (var i = 0; i < result.groups.length; i++) {
                result.groups[i].detail = rootpath + "/groups/" + result.groups[i].id;
                result.groups[i].edit = rootpath + "/groups/" + result.groups[i].id + "/edit";
            }
            $scope.groups = result.groups;
        })

    $scope.deleteGroup = function (id) {
        groupService.deleteOne(id)
            .success(function () {
                for (var i = 0; i < $scope.groups.length; i++) {
                    if ($scope.groups[i].id == id) {
                        $scope.groups.splice(i, 1);
                    }
                }
            });
        }
}]);