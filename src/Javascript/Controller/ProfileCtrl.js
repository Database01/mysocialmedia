app.controller('ProfileCtrl', ['$scope', '$routeParams', 'loginService', '$http', '$rootScope', 'searchService', 'memberService', '$timeout', function ($scope, $routeParams, loginService, $http, $rootScope, searchService, memberService, $timeout) {

    $scope.contacts = [];

    $scope.members = [];

    $scope.editName = function () {
        $scope.nameEdition = true;
    };

    $scope.validateName = function () {
        $scope.nameEdition = false;
        memberService.setName(memberService.getId(), $rootScope.infos.name)
            .success(function (result) {
                localStorage.setItem('infos', JSON.stringify(result.infos));
                $rootScope.infos = result.infos;
            });
    };

    $scope.editEmail = function () {
        $scope.emailEdition = true;
    };

    $scope.validateEmail = function () {
        $scope.emailEdition = false;
        memberService.setEmail(memberService.getId(), $rootScope.infos.email)
            .success(function (result) {
                localStorage.setItem('infos', JSON.stringify(result.infos));
                $rootScope.infos = result.infos;
            });
    };

    memberService.getDashboardStatistics(memberService.getId())
        .success(function (result) {
            $scope.statistics = [];
            if (result.statistics.documents !== undefined) {
                $scope.statistics.push({name: "Documents", data: result.statistics.documents});
            }
            if (result.statistics.medias !== undefined) {
                $scope.statistics.push({name: "Médiathèques", data: result.statistics.medias});
            }
            if (result.statistics.contacts !== undefined) {
                $scope.statistics.push({name: "Contacts", data: result.statistics.contacts});
            }
            if (result.statistics.borrows !== undefined && result.statistics.lends !== undefined) {
                $scope.statistics.push({name: "Emprunts", data: (result.statistics.borrows + result.statistics.lends)});
            }
        });

}]);