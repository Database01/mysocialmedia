app.config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

app.controller('OAuthSigninCtrl', ['$scope', '$location', '$http', 'oAuthSigninService', function ($scope, $location, $http, oAuthSigninService) {

    googleSigninCallback = function (authResult) {
        if (authResult['code']) {
            // Hide the sign-in button now that the user is authorized, for example:
            $('#googleSigninButton').attr('style', 'display: none');
            authResult['g-oauth-window'] = "";

            // Send the code to the server
            oAuthSigninService.googleSignin(authResult, $scope);
        }
    };

    facebookSigninCallback = function (response) {

        if (response.status === "connected") {
            // Hide the sign-in button now that the user is authorized, for example:
            response.state = $scope.facebookInfo.state;

            // Send the code to the server then when authentied, redirect to dashboard
            oAuthSigninService.facebookSignin(response, $scope);
        }

    };

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    checkLoginState = function () {
        FB.getLoginStatus(function (response) {
            facebookSigninCallback(response);
        });
    };

    oAuthSigninService.signin($scope);

}]);