app.controller('MediaCtrl', ['$scope', '$location', 'loginService', '$http', 'mediaService', '$rootScope',function ($scope, $location, loginService, $http, mediaService, $rootScope) {

    $scope.createMedia = rootpath+"/media/add";

    mediaService.getAll()
        .success(function (result) {
            for (var i = 0; i < result.medias.length; i++) {
                result.medias[i].detail = rootpath+"/media/"+result.medias[i].id;
            }
            $scope.medias = result.medias;
        });

    $scope.setPublic = function (mediaId) {
        mediaService.setPublic(mediaId)
            .success(function () {
                $rootScope.$broadcast('alertEvent', {message:"Médiathèque rendue publique", type:"success"});

                for (var i = 0; i < $scope.medias.length; i++) {
                    if($scope.medias[i].id == mediaId) {
                        $scope.medias[i].public = true;
                    }
                }
            });
    };

    $scope.setPrivate = function (mediaId) {
        mediaService.setPrivate(mediaId)
            .success(function () {
                $rootScope.$broadcast('alertEvent', {message:"Médiathèque rendue privée", type:"success"});

                for (var i = 0; i < $scope.medias.length; i++) {
                    if($scope.medias[i].id == mediaId) {
                        $scope.medias[i].public = false;
                    }
                }
            });
    };

    $scope.removeMedia = function (mediaId) {
        mediaService.deleteOne(mediaId)
            .success(function () {
                for (var i = 0; i < $scope.medias.length; i++) {
                    if($scope.medias[i].id == mediaId) {
                        $scope.medias.splice(i, 1);
                    }
                }
                $rootScope.$broadcast('alertEvent', {message:"Médiathèque supprimée avec succès", type:"success"});
            });
    };

}]);