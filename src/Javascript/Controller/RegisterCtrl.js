app.controller('RegisterCtrl', ['$scope', '$rootScope', '$location', 'loginService', function ($scope, $rootScope, $location, loginService) {

    $scope.login = function () {
        $location.path('/login');
    };

    $scope.register = function () {
        if($scope.form.$valid !== true) {
            $rootScope.$broadcast('alertEvent', {message: "Le formulaire n'est pas valide", type:"danger"});
            return;
        }

        if($scope.user.password !== $scope.password) {
            $rootScope.$broadcast('alertEvent', {message: "Les mots de passes ne correspondent pas", type:"danger"});
            return;
        }

        loginService.register($scope.user, $scope)
            .success(function (result) {
                localStorage.setItem('infos', JSON.stringify(result.infos));
                $location.path('/dashboard');
            })
            .error(function (e) {
                $rootScope.$broadcast('alertEvent', {message: e.message, type:"danger"});
            });
    };

}]);