app.controller('WishlistCtrl',['$scope', '$rootScope', '$http', '$routeParams', 'memberService', 'wishService', function ($scope, $rootScope, $http, $routeParams, memberService, wishService) {

    wishService.getAll(memberService.getId())
        .success(function(result){
            $scope.documents = result.documents;
        }
    );

    $scope.addToCollection = function (id) {
        wishService.addToCollection(memberService.getId(), {document:id})
            .success(function () {
                for (var i = 0; i < $scope.documents.length; i++) {
                    if($scope.documents[i].id === id) {
                        $scope.documents.splice(i, 1);
                    }
                }
                $rootScope.$broadcast('alertEvent', {message:"Souhait ajouté à vos documents", type:"success"});
            });
    };

    $scope.deleteOne = function (id) {
        wishService.deleteOne(memberService.getId(), id)
            .success(function () {
                for (var i = 0; i < $scope.documents.length; i++) {
                    if($scope.documents[i].id === id) {
                        $scope.documents.splice(i, 1);
                    }
                }
                $rootScope.$broadcast('alertEvent', {message:"Souhait supprimé", type:"success"});
            });
    };

}]);