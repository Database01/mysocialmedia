app.controller('LoginTwitterCtrl', ['$scope', '$location', 'oAuthSigninService', function ($scope, $location, oAuthSigninService) {

    if ($location.search().oauth_token && $location.search().oauth_verifier) {
       $param = $location.search();
       $location.url($location.path());
       oAuthSigninService.twitterSignin($param, $scope);
    }else{
        $location.path('/login');
    }
}]);