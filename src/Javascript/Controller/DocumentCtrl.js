app.controller('DocumentCtrl', ['$scope', '$rootScope', '$location', '$routeParams', 'loginService', 'documentService', 'documentTypeService', 'searchService', 'wishService','memberService', '$timeout', 'mediaService', function ($scope, $rootScope, $location, $routeParams, loginService, documentService, documentTypeService, searchService, wishService,memberService, $timeout, mediaService) {

    $scope.addDocument = rootpath + "/document/add";

    $scope.latestDocuments = [];

    $scope.wishlist = [];

    $scope.myDocuments = [];

    $scope.showModal = false;

    if ($routeParams.q !== undefined) {
        $scope.search = $routeParams.q;
    } else {
        $scope.search = "";
    }

    if ($routeParams.f !== undefined) {
        $scope.filter = $routeParams.f;
    } else {
        $scope.filter = 0;
    }

    mediaService.getAll()
        .success(function (result) {
            $scope.collections = result.medias;
        });

    $scope.addAmazonDocument = function (isbn) {
        documentService.addAmazonDocument(isbn)
            .success(function (res) {
                var id = res.result.id;
                $scope.addToCollection(id);
            })
            .error(function () {
                $rootScope.$broadcast('alertEvent', {message:"Vous possédez déjà ce document", type:"danger"});
            });
    };

    $scope.addWishAmazonDocument = function (isbn) {
        documentService.addAmazonDocument(isbn)
            .success(function (res) {
                var id = res.result.id;
                $scope.addWish(id);
            })
            .error(function () {
                $rootScope.$broadcast('alertEvent', {message:"Ce document fait déjà partie de vos souhaits", type:"danger"});
            });
    };

    $scope.limitLatestDocuments = 12;

    documentService.getLatest().success(function (result) {
        for (var i = 0; i < result.documents.length; i++) {
            result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
        }
        $scope.latestDocuments = result.documents;
        $scope.latestDocumentsFullyLoaded = true;
    });

    $scope.moreLatestDocuments = function () {
        $scope.limitLatestDocuments = 24;
    };

    $scope.lessLatestDocuments = function () {
        $scope.limitLatestDocuments = 12;
    };

    getMines = function (from) {
        $scope.myDocumentsFullyLoaded = false;
        var query = {};

        if (from === undefined) {
            query = memberService.getMines(memberService.getId());
        } else {
            query = memberService.getMines(memberService.getId(), from);
        }

        query.success(function (result) {
            $scope.myDocumentsLoaded = true;
            for (var i = 0; i < result.documents.length; i++) {
                result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
                $scope.myDocuments.push(result.documents[i]);
            }
            $scope.myDocuments.count = result.count;
            $scope.myDocuments.canLoad = (result.documents.length !== 0);

            $scope.myDocumentsFullyLoaded = true;
        });
    };

    getMines();

    $scope.moreMyDocuments = function () {
        getMines($scope.myDocuments.length);
    };

    var addDoc = function (docs, id) {
        for (var i = 0; i < docs.length; i++) {
            if(docs[i].id === id) {
                docs[i].inDocuments = true;
                break;
            }
        }
    };

    var deleteDoc = function (docs, id) {
        for (var i = 0; i < docs.length; i++) {
            if(docs[i].id === id) {
                delete docs[i].inDocuments;
                break;
            }
        }
    };

    var addDocumentLoading = function (docs, id, bool) {
        if(docs !== undefined) {
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].id === id) {
                    docs[i].addDocumentLoading = bool;
                    break;
                }
            }
        }
    };

    var removeDocumentLoading = function (docs, id, bool) {
        if(docs !== undefined) {
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].id === id) {
                    docs[i].removeDocumentLoading = bool;
                    break;
                }
            }
        }
    };

    $scope.addToCollection = function (id) {
        addDocumentLoading($scope.documents, id, true);
        addDocumentLoading($scope.latestDocuments, id, true);
        memberService.addDocument(memberService.getId(), id)
            .success(function (result) {
                $scope.myDocuments.count++;
                $scope.myDocuments.push(result.documents);
                $rootScope.$broadcast('alertEvent', {message:"Document ajouté avec succès", type:"success"});
                if($scope.latestDocuments !== undefined) {
                    addDoc($scope.latestDocuments, id);
                }
                if($scope.documents !== undefined) {
                    addDoc($scope.documents, id);
                }
            })
            .error(function () {
                $rootScope.$broadcast('alertEvent', {message:"Vous possédez déjà ce document", type:"danger"});
            })
            .then(function () {
                addDocumentLoading($scope.documents, id, false);
                addDocumentLoading($scope.latestDocuments, id, false);
            });
    };

    $scope.removeFromCollection = function (id) {
        removeDocumentLoading($scope.documents, id, true);
        removeDocumentLoading($scope.latestDocuments, id, true);
        removeDocumentLoading($scope.myDocuments, id, true);
        memberService.removeDocument(memberService.getId(), id)
            .success(function () {
                var docs = $scope.myDocuments;
                for (var i = 0; i < docs.length; i++) {
                    if(docs[i].id === id) {
                        docs.count--;
                        docs.splice(i, 1);
                        break;
                    }
                }

                $rootScope.$broadcast('alertEvent', {message:"Document supprimé avec succès", type:"success"});

                if($scope.latestDocuments !== undefined) {
                    deleteDoc($scope.latestDocuments, id);
                }
                if($scope.documents !== undefined) {
                    deleteDoc($scope.documents, id);
                }
            })
            .error(function (e) {
                $rootScope.$broadcast('alertEvent', {message: e.message, type:"danger"});
            })
            .then(function () {
                removeDocumentLoading($scope.documents, id, false);
                removeDocumentLoading($scope.latestDocuments, id, false);
                removeDocumentLoading($scope.myDocuments, id, false);
            });
    };

    /*-----------------------*\
             SEARCH PART
    \*-----------------------*/

    $scope.$watch('filter', function () {
        $location.search({f: $scope.filter, q: $scope.search});
    });

    launchSearch = function () {
        if ($scope.search.length === 0) {
            $location.search({f: $scope.filter, q: null});
        } else {
            $location.search({f: $scope.filter, q: $scope.search});
        }
        if ($scope.search.length > 3) {
            $scope.searchDocumentsFullyLoaded = false;
            $scope.searchStarted = true;
            $scope.searchLaunched = true;
            searchService.documentSearch({search: $scope.search})
                .success(function (result) {
                    $scope.searchStarted = false;
                    $scope.documents = {};
                    for (var i = 0; i < result.documents.length; i++) {
                        result.documents[i].detail = rootpath + "/document/" + result.documents[i].id;
                    }
                    $scope.documents = result.documents;
                    $scope.searchDocumentsFullyLoaded = true;
                    $scope.documents.canLoad = (result.documents.length !== 0);
                    $scope.amazonSearchAllowed = true;
                });
        }
    };

    var promise = $timeout(launchSearch, 1000);

    $scope.$watch('search', function () {
        $timeout.cancel(promise);
        promise = $timeout(launchSearch, 1000);
    }, true);


    /*-----------------------*\
             AMAZON PART
    \*-----------------------*/

    $scope.toggleModal = function (amazonDocument) {
        $scope.amazonDetail = amazonDocument;
        $scope.showModal = !$scope.showModal;
    };

    $scope.amazonSearch = function (search, filter) {
        $scope.amazonDocuments = [];
        $scope.amazonSearchLaunched = true;
        $scope.amazonSearchStarted = true;
        searchService.amazonSearch({search: $scope.search, filter: filter})
            .success(function (result) {
                $scope.amazonSearchStarted = false;
                $scope.amazonDocuments = [];
                if (result.Books != undefined) {
                    for (var i = 0; i < result.Books.length; i++) {
                        book = {
                            name: result.Books[i].ItemAttributes.Title,
                            type: "Livre",
                            isbn: result.Books[i].ItemAttributes.ISBN,
                            ean: result.Books[i].ItemAttributes.EAN,
                            full: result.Books[i]
                        };
                        $scope.amazonDocuments.push(book);
                    }
                }
                if (result.DVD != undefined) {
                    for (var i = 0; i < result.DVD.length; i++) {
                        DVD = {
                            name: result.DVD[i].ItemAttributes.Title,
                            type: "DVD",
                            isbn: result.DVD[i].ItemAttributes.ISBN,
                            ean: result.DVD[i].ItemAttributes.EAN,
                            full: result.DVD[i]
                        };
                        $scope.amazonDocuments.push(DVD);
                    }
                }
                if (result.Music != undefined) {
                    for (var i = 0; i < result.Music.length; i++) {
                        Music = {
                            name: result.Music[i].ItemAttributes.Title,
                            type: "Musique",
                            isbn: result.Music[i].ItemAttributes.ISBN,
                            ean: result.Music[i].ItemAttributes.EAN,
                            full: result.Music[i]
                        };
                        $scope.amazonDocuments.push(Music);
                    }
                }
                if (result.VideoGames != undefined) {
                    for (var i = 0; i < result.VideoGames.length; i++) {
                        VideoGames = {
                            name: result.VideoGames[i].ItemAttributes.Title,
                            type: "Jeux Vidéo",
                            isbn: result.VideoGames[i].ItemAttributes.ISBN,
                            ean: result.VideoGames[i].ItemAttributes.EAN,
                            full: result.VideoGames[i]
                        };
                        $scope.amazonDocuments.push(VideoGames);
                    }
                }
            })
            .error(function () {
                console.log("Error Search : " + search + " - Filter : " + filter);
                $scope.amazonSearchStarted = false;
                $scope.amazonSearchLaunched = false;
            });
    };

    /*-----------------------*\
             WISH PART
    \*-----------------------*/

    wishService.getAll(memberService.getId())
        .success(function(result){
            $scope.wishlist = result.documents;
            $scope.wishlistFullyLoaded = true;
        }
    );

    var wishLoading = function (docs, id, bool) {
        if(docs !== undefined) {
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].id === id) {
                    docs[i].wishLoading = bool;
                    break;
                }
            }
        }
    };

    var setInWishList = function (docs, id, bool) {
        if(docs !== undefined) {
            for (var i = 0; i < docs.length; i++) {
                if (docs[i].id === id) {
                    docs[i].inWishlist = bool;
                    break;
                }
            }
        }
    };

    $scope.addWish = function(id){
        wishLoading($scope.documents, id, true);
        wishLoading($scope.latestDocuments, id, true);
        wishService.addOne(memberService.getId(),{document:id})
            .success(function(){
                setInWishList($scope.documents, id, true);
                setInWishList($scope.latestDocuments, id, true);
            })
            .error(function () {
                $rootScope.$broadcast('alertEvent', {message:"Impossible de souhaiter un document que vous possédez déjà", type:"danger"});
            })
            .then(function(){
                wishLoading($scope.documents, id, false);
                wishLoading($scope.latestDocuments, id, false);
            });
    };

    $scope.removeWish = function (id) {
        wishLoading($scope.documents, id, true);
        wishLoading($scope.latestDocuments, id, true);
        wishService.deleteOne(memberService.getId(), id)
            .success(function(){
                setInWishList($scope.documents, id, false);
                setInWishList($scope.latestDocuments, id, false);
            })
            .then(function(){
                wishLoading($scope.documents, id, false);
                wishLoading($scope.latestDocuments, id, false);
            });
    };


    /*-----------------------*\
             WATCHERS PART
    \*-----------------------*/

    $scope.wishlistFullyLoaded = false;

    $scope.latestDocumentsFullyLoaded = false;

    $scope.searchDocumentsFullyLoaded = false;

    $scope.myDocumentsFullyLoaded = false;

    $scope.$watchGroup(['myDocumentsFullyLoaded', 'latestDocumentsFullyLoaded'], function (newValue) {
        if(newValue[0] && newValue[1]) {
            for (var i = 0; i < $scope.latestDocuments.length; i++) {
                for (var j = 0; j < $scope.myDocuments.length; j++) {
                    if($scope.latestDocuments[i].id === $scope.myDocuments[j].id) {
                        $scope.latestDocuments[i].inDocuments = true;
                        break;
                    }
                }
            }
        }
    });
    $scope.$watchGroup(['myDocumentsFullyLoaded', 'searchDocumentsFullyLoaded'], function (newValue) {
        if(newValue[0] && newValue[1]) {
            for (var i = 0; i < $scope.documents.length; i++) {
                for (var j = 0; j < $scope.myDocuments.length; j++) {
                    if($scope.documents[i].id === $scope.myDocuments[j].id) {
                        $scope.documents[i].inDocuments = true;
                        break;
                    }
                }
            }
        }
    });
    $scope.$watchGroup(['wishlistFullyLoaded', 'latestDocumentsFullyLoaded'], function (newValue) {
        if(newValue[0] && newValue[1]) {
            for (var i = 0; i < $scope.latestDocuments.length; i++) {
                for (var j = 0; j < $scope.wishlist.length; j++) {
                    if($scope.latestDocuments[i].id === $scope.wishlist[j].id) {
                        $scope.latestDocuments[i].inWishlist = true;
                        break;
                    }
                }
            }
        }
    });
    $scope.$watchGroup(['wishlistFullyLoaded', 'searchDocumentsFullyLoaded'], function (newValue) {
        if(newValue[0] && newValue[1]) {
            for (var i = 0; i < $scope.documents.length; i++) {
                for (var j = 0; j < $scope.wishlist.length; j++) {
                    if($scope.documents[i].id === $scope.wishlist[j].id) {
                        $scope.documents[i].inWishlist = true;
                        break;
                    }
                }
            }
        }
    });

}]);
