app.controller('DocumentKindCtrl', ['$scope', '$location', 'loginService', '$http', 'documentKindService', function ($scope, $location, loginService, $http, documentKindService) {

    $scope.createDocumentKind = rootpath+"/document/kind/add";

    documentKindService.getAll()
        .success(function (result) {
            for (var i = 0; i < result.kinds.length; i++) {
                result.kinds[i].edit = rootpath+"/document/kind/"+result.kinds[i].id+"/edit";
                result.kinds[i].delete = rootpath+"/document/kind/"+result.kinds[i].id+"/delete";
            }
            $scope.kinds = result.kinds;
        });

}]);