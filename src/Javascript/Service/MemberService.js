app.factory('memberService', ['$http', '$location', '$rootScope', function($http, $location, $rootScope) {
    return {
        getAll: function() {
            return $http.get(rootpath+"/api/members");
        },
        getOne: function (id) {
            return $http.get(rootpath+'/api/members/'+id);
        },
        getAllContacts: function (memberId) {
            return $http.get(rootpath+'/api/members/'+memberId+'/contacts');
        },
        getLatestContacts: function (memberId) {
            return $http.get(rootpath+'/api/members/'+memberId+'/contacts/latest');
        },
        addContact: function (contactId, memberId) {
            return $http.post(rootpath+'/api/members/'+memberId+'/contacts/'+contactId+'/add');
        },
        removeContact: function (contactId, memberId) {
            return $http.delete(rootpath+'/api/members/'+memberId+'/contacts/'+contactId);
        },
        getId: function () {
            var infos = JSON.parse(localStorage.getItem('infos'));
            return infos.id;
        },
        getAdmin: function () {
            var infos = JSON.parse(localStorage.getItem('infos'));
            if(infos.admin === null)
                return false;
            return infos.admin;
        },
        getName: function () {
            var infos = JSON.parse(localStorage.getItem('infos'));
            return infos.name;
        },
        getEmail: function () {
            var infos = JSON.parse(localStorage.getItem('infos'));
            return infos.email;
        },
        getEmailMd5: function () {
            var infos = JSON.parse(localStorage.getItem('infos'));
            return infos.emailMd5;
        },
        getDocuments: function (memberId) {
            return $http.get(rootpath+'/api/members/'+memberId+'/documents');
        },
        getMines: function(memberId, from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath+'/api/members/'+memberId+'/documents/latest', param);
        },
        getLatestMines: function(memberId) {
            return $http.post(rootpath+'/api/members/'+memberId+'/documents/latest/partial');
        },
        getLatestLoans: function (id) {
            return $http.get(rootpath+"/api/members/loans/"+id+"/latest");
        },
        getDashboardStatistics: function (memberId) {
            return $http.get(rootpath + '/api/members/' + memberId + '/statistics/dashboard');
        },
        getPublicStatistics: function (memberId) {
            return $http.get(rootpath + '/api/members/' + memberId + '/statistics/public');
        },
        getPublicMedias: function (memberId) {
            return $http.get(rootpath + '/api/members/' + memberId + '/medias/public');
        },
        addDocument: function (memberId, documentId) {
            return $http.post(rootpath+'/api/members/'+memberId+'/documents/'+documentId+'/add');
        },
        removeDocument: function (memberId, documentId) {
            return $http.post(rootpath+'/api/members/'+memberId+'/documents/'+documentId+'/remove');
        },
        getLatestComments: function(memberid){
            return $http.get(rootpath + '/api/members/'+ memberid + '/comments/latest');
        },
        rootInfo: function () {

            $rootScope.rootPath = rootpath;
            $rootScope.connected = true;
            $rootScope.media = rootpath+"/media";
            $rootScope.member = rootpath+"/member";
            $rootScope.social = rootpath+"/social";
            $rootScope.document = rootpath+"/document";
            $rootScope.documentType = rootpath+"/document/type";
            $rootScope.documentKind = rootpath+"/document/kind";
            $rootScope.logout = rootpath+"/logout";
            $rootScope.dashboard = rootpath+"/dashboard";
            $rootScope.loan = rootpath+"/loan";
            $rootScope.profile = rootpath+"/profile";
            $rootScope.wishlistProfile = rootpath+"/wishlist";

            $rootScope.menus = [
                {
                    name:"Dashboard",
                    icon:"fa-dashboard",
                    link:$rootScope.dashboard,
                    regexp:'^/dashboard*.'
                },{
                    name:"Documents",
                    icon:"fa-book",
                    link:$rootScope.document,
                    regexp:'^/document*.'
                },{
                    name:"Collections",
                    icon:"fa-bookmark",
                    link:$rootScope.media,
                    regexp:'^/media*.'
                },{
                    name:"Prêts",
                    icon:"fa-exchange",
                    link:$rootScope.loan,
                    regexp:'^/loan*.'
                },{
                    name:"Social",
                    icon:"fa-connectdevelop",
                    link:$rootScope.social,
                    regexp:'^/social|groups*.'
                },{
                    name:"Profil",
                    icon:"fa-user",
                    link:$rootScope.profile,
                    regexp:'^/profile*.'
                },{
                    name:"Souhaits",
                    icon:"fa-heart",
                    link:$rootScope.wishlistProfile,
                    regexp:'^/wishlist*.'
                }
            ];

            for (var i = 0; i < $rootScope.menus.length; i++) {
                if($rootScope.menus[i].regexp != undefined) {
                    regexp = new RegExp($rootScope.menus[i].regexp, "i");
                    if(regexp.test($location.path()) === true) {
                        $rootScope.menus[i].active = true;
                        break;
                    }
                }
            }

            var infos = JSON.parse(localStorage.getItem('infos'));
            $rootScope.infos = infos;

            return true;
        },
        setName: function (id, value) {
            return $http.post(rootpath+'/api/members/'+id+'/name', {name:value});
        },
        setEmail: function (id, value) {
            return $http.post(rootpath+'/api/members/'+id+'/email', {mail:value});
        },
        getLatestActivities: function (memberId) {
            return $http.get(rootpath + '/api/activity/'+memberId);
        }
    }
}]);