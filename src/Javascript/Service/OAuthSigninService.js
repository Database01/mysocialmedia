app.factory('oAuthSigninService', ['$http', '$location', function ($http, $location) {
    return {

        googleSignin: function (data, scope) {
            $http.post(rootpath + '/oauth/signin/google', data)
                .success(function (result) {
                    localStorage.setItem('infos', JSON.stringify(result.infos));
                    $location.path('/dashboard');
                });
        },

        facebookSignin: function (data, scope) {
            $http.post(rootpath + '/oauth/signin/facebook', data)
                .success(function (result) {
                    localStorage.setItem('infos', JSON.stringify(result.infos));
                    $location.path('/dashboard');
                })
        },

        twitterSignin: function (data, scope) {
            $http.post(rootpath + '/oauth/signin/twitter', data)
                .success(function (result) {
                    localStorage.setItem('infos', JSON.stringify(result.infos));
                    $location.path('/dashboard');
                })
        },

        signin: function (scope) {
            $http.get(rootpath + '/oauth/signin')
                .then(function (result) {
                    // Get CLIENT_ID and the state token
                    scope.googleInfo = result.data.results.google;
                    scope.twitterInfo = result.data.results.twitter;
                    scope.facebookInfo = result.data.results.facebook;

                    // Import the JS necessary for the G+ button
                    (function () {
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = 'https://apis.google.com/js/client:plusone.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();

                    // Import the JS necessary for the Fb button

                    fbAsyncInit = function () {
                        FB.init({
                            appId: scope.facebookInfo.CLIENT_ID,
                            cookie: true,  // enable cookies to allow the server to access
                                           // the session
                            status: true,
                            xfbml: true,  // parse social plugins on this page
                            oauth: true,
                            version: 'v2.2' // use version 2.2
                        });
                        FB.getLoginStatus(function (response) {
                            facebookSigninCallback(response);
                        }, true);
                    };

                    FB = null;

                    (function () {
                        var po = document.createElement('script');
                        po.type = 'text/javascript';
                        po.async = true;
                        po.src = '//connect.facebook.net/fr_FR/sdk.js';
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(po, s);
                    })();
                });
        }
    }
}]);