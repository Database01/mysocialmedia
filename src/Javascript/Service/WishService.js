app.factory('wishService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function(id) {
            return $http.get(rootpath+"/api/members/"+id+'/wishes');
        },
        getOne: function (id) {
            return $http.get(rootpath+"/api/members/"+id+'/wishes');
        },
        addOne: function (id, document) {
            return $http.post(rootpath+"/api/members/"+id+'/wishes', document);
        },
        addToCollection: function (id, document) {
            return $http.post(rootpath+"/api/members/"+id+'/wishes/collection', document);
        },
        deleteOne: function (id, document) {
            return $http.delete(rootpath+"/api/members/"+id+'/wishes/'+document);
        }

    }
}]);