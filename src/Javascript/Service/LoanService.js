app.factory('loanService', ['$http', '$location', function($http, $location) {

    return {
        getAll: function() {
            return $http.get(rootpath+"/api/loans");
        },
        getOne: function(id) {
            return $http.get(rootpath+"/api/loans/"+id);
        },
        addOne: function (parameters) {
            return $http.post(rootpath+'/api/loans/add', parameters);
        },
        editOne: function (id, parameters) {
            return $http.post(rootpath+'/api/loans/'+id+'/edit', parameters);
        },
        deleteOne: function (id) {
            return $http.post(rootpath+'/api/loans/'+id+'/delete');
        }

    }
}]);