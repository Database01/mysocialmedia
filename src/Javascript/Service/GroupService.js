app.factory('groupService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function() {
            return $http.get(rootpath+"/api/groups");
        },
        getOne: function(id) {
            return $http.get(rootpath+'/api/groups/'+id);
        },
        add: function(data) {
            return $http.post(rootpath+'/api/groups/add', data);
        },
        editOne: function(id, group) {
            return $http.post(rootpath+'/api/groups/'+id+'/edit', group);
        },
        deleteOne: function (id) {
            return $http.delete(rootpath + "/api/groups/" + id);
        },
        getPosts: function (id) {
            return $http.get(rootpath + "/api/groups/" + id + "/posts");
        },
        getLatestPosts: function (id, from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath + "/api/groups/" + id + "/posts/latest", param);
        },
        editPost: function(id_group, id_post, message) {
            return $http.post(rootpath + "/api/groups/" + id_group + "/posts/" + id_post + "/edit", message);
        },
        deletePost: function(id_group, id_post) {
            return $http.delete(rootpath + "/api/groups/" + id_group + "/posts/" + id_post);
        },
        addPost: function(id_group, post) {
            return $http.post(rootpath + "/api/groups/" + id_group + "/posts/add", post);
        },
        getMembers: function(id_group) {
            return $http.get(rootpath + "/api/groups/" + id_group + "/members");
        },
        addMember: function(id_group, member) {
            member = {member: member};
            return $http.post(rootpath + "/api/groups/" + id_group + "/members/add", member);
        },
        getLatestMembers: function (id, from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath + "/api/groups/" + id + "/members", param);
        },
        getOneMember: function (id_group, id_member) {
            return $http.get(rootpath + "/api/groups/" + id_group + "/members/" + id_member);
        },
        deleteMember: function(id_group, id_member) {
            return $http.delete(rootpath + "/api/groups/" + id_group + "/members/" + id_member);
        },
        addBanned: function(id_group, params) {
            return $http.post(rootpath + "/api/groups/" + id_group + "/banned/add", params);
        },
        getLatestBanned: function (id, from, count) {
            var param = {};
            if(from !== undefined) {
                param.from = from;
            }
            if(count !== undefined) {
                param.count = count;
            }

            return $http.post(rootpath + "/api/groups/" + id + "/banned", param);
        },
        deleteBanned: function(id_group, id_ban) {
            return $http.delete(rootpath + "/api/groups/" + id_group + "/banned/" + id_ban);
        },
        getTypes: function() {
            return $http.get(rootpath + "/api/groups/types");
        }

    }
}]);