app.factory('mediaService', ['$http', '$location', function($http, $location) {
    return {
        getAll: function() {
            return $http.get(rootpath+"/api/medias");
        },
        getOne: function (id) {
            return $http.get(rootpath+'/api/medias/'+id);
        },
        addOne: function (media) {
            return $http.post(rootpath+'/api/medias/add', media);
        },
        editOne: function (id, media) {
            return $http.post(rootpath+'/api/medias/'+id+'/edit', media);
        },
        addDocument: function (id, documentId) {
            return $http.post(rootpath+'/api/medias/'+id+'/document/'+documentId+'/add');
        },
        removeDocument: function (id, documentId) {
            return $http.post(rootpath+'/api/medias/'+id+'/document/'+documentId+'/remove');
        },
        deleteOne: function (id) {
            return $http.delete(rootpath+'/api/medias/'+id+'/delete');
        },
        setType: function (id, typeId) {
            return $http.post(rootpath+'/api/medias/'+id+'/type', {type: typeId});
        },
        setPrivate: function (id) {
            return $http.post(rootpath+'/api/medias/'+id+'/private');
        },
        setPublic: function (id) {
            return $http.post(rootpath+'/api/medias/'+id+'/public');
        },
        setName: function (id, name) {
            return $http.post(rootpath+'/api/medias/'+id+'/name', {name: name});
        }

    }
}]);