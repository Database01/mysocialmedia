app.directive('keySubmit', function() {
    return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
            return elem.bind('keydown', function(event) {
                var code;
                code = event.keyCode || event.which;
                if (code === 13 && event.metaKey) {
                    return scope.$apply(attrs.keySubmit);
                }
            });
        }
    };
});
